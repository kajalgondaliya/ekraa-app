import 'package:ekraa_app/model/chapter/chapters.dart';

final allChapters = [
  Chapter(name: "Chapter", number: "1"),
  Chapter(name: "Chapter", number: "2"),
  Chapter(name: "Chapter", number: "3"),
  Chapter(name: "Chapter", number: "4"),
  Chapter(name: "Chapter", number: "5"),
  Chapter(name: "Chapter", number: "6"),
];
