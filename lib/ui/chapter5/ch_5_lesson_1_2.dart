import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch5Page1 extends StatefulWidget {
  final Chapter6Model model1;
  final HorofClickCallback onTap;
  final fromLesson;
  final title1;

  const Ch5Page1({
    Key key,
    this.model1,
    this.onTap,
    this.fromLesson,
    this.title1,
  }) : super(key: key);

  @override
  _Ch5Page1State createState() => _Ch5Page1State();
}

class _Ch5Page1State extends State<Ch5Page1> {
  @override
  void initState() {
    // widget.fromLesson will return 1 or 2 now this will be very essential for us to know which title and data and list belongs to which lesson
    // since we are managing 2 lessons in one page because
    // it has similar UI's so to duplicate code we are managing 2 lessons in one page by just passing one flag that is == widget.fromLesson
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 20),
            getHeadersLesson1(),
            SizedBox(height: 20),
            widget.fromLesson == "1"
                ? getWordsRow(widget.model1.filler.getRange(0, 5).toList())
                : getWordsRow(widget.model1.filler.getRange(0, 7).toList()),
            getTable(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getHeadersLesson1() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 10),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          widget.fromLesson == "1"
              ? getImage('assets/images/hand.png')
              : SizedBox.shrink(),
          Text(
            widget.title1,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: ThemeColors.colorBorderCoffee,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w900,
            ),
          ),
          Text(
            widget.model1.title,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: ThemeColors.colorRose,
              fontFamily: 'Uthmanic',
              fontSize: textSize + 5,
              fontWeight: FontWeight.w900,
            ),
          ),
          widget.fromLesson == "1"
              ? getImage2('assets/images/icon5_1.png')
              : getImage2('assets/images/icon5.png'),
        ],
      ),
    );
  }

  getImage(String imageUrl) {
    return Image.asset(
      imageUrl,
      width: kDeviceType == DeviceScreenType.Mobile ? 60 : 110,
      height: kDeviceType == DeviceScreenType.Mobile ? 60 : 110,
      fit: BoxFit.fitHeight,
    );
  }

  getImage2(String imageUrl) {
    return Image.asset(
      imageUrl,
      width: kDeviceType == DeviceScreenType.Mobile ? 60 : 110,
      height: kDeviceType == DeviceScreenType.Mobile ? 60 : 110,
      fit: BoxFit.fitWidth,
    );
  }

  getTable() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(width: 1, color: Colors.black),
          left: BorderSide(width: 1, color: Colors.black),
          right: BorderSide(width: 1, color: Colors.black),
        ),
      ),
      child: widget.fromLesson == "1" ? getTable1() : getTable2(),
    );
  }

  getTable1() {
    return Column(
      children: <Widget>[
        getRow(widget.model1.filler.getRange(5, 10).toList()),
        getRow(widget.model1.filler.getRange(10, 15).toList()),
        getRow(widget.model1.filler.getRange(15, 20).toList()),
        getRow(widget.model1.filler.getRange(20, 25).toList()),
        getRow(widget.model1.filler.getRange(25, 30).toList()),
        getRow(widget.model1.filler.getRange(30, 35).toList()),
        getRow(widget.model1.filler.getRange(35, 40).toList()),
        getRow(widget.model1.filler.getRange(40, 45).toList()),
      ],
    );
  }

  getTable2() {
    return Column(
      children: <Widget>[
//        getRow(widget.model1.filler.getRange(5, 10).toList()),
        getRow(widget.model1.filler.getRange(8, 13).toList()),
        getRow(widget.model1.filler.getRange(13, 18).toList()),
        getRow(widget.model1.filler.getRange(18, 23).toList()),
        getRow(widget.model1.filler.getRange(23, 28).toList()),
        getRow(widget.model1.filler.getRange(28, 33).toList()),
        getRow(widget.model1.filler.getRange(33, 38).toList()),
        getRow(widget.model1.filler.getRange(38, 43).toList()),
        getRow(widget.model1.filler.getRange(43, 48).toList()),
      ],
    );
  }

  getWordsRow(List<Filler> row) {
    return Container(
      alignment: Alignment.center,
      height: widget.fromLesson == "1"
          ? kOrientation ? heightTable * 2 : heightTable * 3
          : kOrientation ? heightTable * 1.5 : heightTable * 3,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: row.length,
        reverse: true,
        itemBuilder: (context, index) {
          return itemWord(row[index]);
        },
      ),
    );
  }

  getRow(List<dynamic> filler) {
    return Container(
      height: kOrientation ? heightTable : heightTable + 10,
      child: ListView.builder(
        itemCount: filler.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(filler[index], filler.length, index);
        },
      ),
    );
  }

  itemWord(Filler horof) {
    return GestureDetector(
      onTap: () {
        widget.onTap(horof);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 1.5, vertical: 3),
            padding: EdgeInsets.only(top: 5, right: 5),
            width: widget.fromLesson == "1"
                ? kOrientation ? heightWords * 1.5 : heightWords * 2
                : kOrientation ? heightWords + 4.5 : heightWords * 1.8,
            height: widget.fromLesson == "1"
                ? kOrientation ? heightWords * 1.5 : heightWords * 2
                : kOrientation ? heightWords + 4.5 : heightWords * 1.8,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/shape.png'),
                fit: BoxFit.fill,
              ),
//            border: Border.all(color: ThemeColors.colorBrown),
//            borderRadius: BorderRadius.circular(3),
            ),
            child: Center(
              child: Text(
                horof.alpha.trim(),
                textDirection: TextDirection.rtl,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.blue[800],
                  fontFamily: 'Uthmanic',
                  fontSize: textSize,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  itemTable(Filler horof, int size, int index) {
//    print('index ===>>> $index');

    return InkWell(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.only(top: 3),
        width: (MediaQuery.of(context).size.width - 40) / size,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(width: 1, color: Colors.black),
            bottom: BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
