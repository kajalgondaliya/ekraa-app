import 'dart:convert';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:ekraa_app/model/chapter/chapter2/chapter2Model.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/ui/base_layout/base_home_layout.dart';
import 'package:ekraa_app/ui/chapter3/ch_3_lesson_1_2_3.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:toast/toast.dart';

class Chapter3 extends StatefulWidget {
  @override
  _Chapter3State createState() => _Chapter3State();
}

class _Chapter3State extends State<Chapter3> {
  var _pageController = PageController();
  int page = 0;

  var currentLesson = "1";
  double currentTotalDuration = 0.0;
  double currentDuration = 0.0;

  String toastMsg = 'Audio is not avaiable.';

  List<dynamic> allLessons;

  LessonsAll lesson1;
  LessonsAll lesson2;
  LessonsAll lesson3;

  AudioCache audioCache = AudioCache();
  bool isCompleted = false;

  refresh() {
    SchedulerBinding.instance.addPostFrameCallback((callback) {
      if (mounted) setState(() {});
    });
  }

  @override
  void initState() {
//    _assetsAudioPlayer.playlistAudioFinished.listen((data) {
//      print("finished : $data");
//    });

    audioCache.fixedPlayer = AudioPlayer();
    audioCache.fixedPlayer.onPlayerCompletion.listen((event) {
      print('complete event called ==>>');
      isCompleted = true;
      refresh();
    });
    audioCache.fixedPlayer.onDurationChanged.listen((data) {
//      print("current : ${data.duration}.");
//      print("current : ${data.duration}.");
      currentTotalDuration = durationToDouble(data);
      print('currentTotalDuration : ${durationToDouble(data)}');
    });

    loadJsonFile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // This function will set parameters device width,height,device_type,current_orientation,dynamic {textSize,heightTable,heightWords} according to screen size.
    // to know which variable holds which value please go to function declaration.
    // this is very essential as we are calculating above values dynamically according to device type and device orientation.
    setDeviceLayoutSizes(context);
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        body: BaseLayout(
          menuIcon: Icons.arrow_back,
          title1: 'Chapter',
          title2: ' 3',
          onTapHome: () {
            stopMusicPlayerAndGoBack();
          },
          onTapMenu: () {
            stopMusicPlayerAndGoBack();
          },
          child: getMainLayout(MediaQuery.of(context)),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() async {
    audioCache.fixedPlayer.stop();
    return true; // return true if the route to be popped
  }

  stopMusicPlayerAndGoBack() {
    audioCache.fixedPlayer.stop();
    Navigator.of(context).pop();
  }

  loadJsonFile() async {
    await rootBundle.loadString('assets/json/chapter3.json').then((onValue) {
      allLessons = jsonDecode(onValue);

      lesson1 = LessonsAll.fromJson(allLessons[0]);
      lesson2 = LessonsAll.fromJson(allLessons[1]);
      lesson3 = LessonsAll.fromJson(allLessons[2]);

      // above 3 list are main list of chapter 2 which is lesson 1,lesson 2,lesson 3...

      refresh();
    });
  }

  getAppBar(statusBarHeight) {
    return Container(
      height: 90,
      padding: EdgeInsets.only(top: statusBarHeight),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_1.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Stack(
        children: [
          IconButton(
            iconSize: 30,
            padding: EdgeInsets.only(top: 15),
            onPressed: () {
              audioCache.fixedPlayer.stop();
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.arrow_back,
              color: ThemeColors.colorBrown,
            ),
          ),
          Center(
            child: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: 'Chapter',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Uthmanic',
                      fontSize: 20,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  TextSpan(
                    text: ' 2',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Uthmanic',
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  getMainLayout(MediaQueryData mediaQueryData) {
    return Container(
      alignment: Alignment.center,
      width: mediaQueryData.size.width,
      height: mediaQueryData.size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/white_page.jpg'),
          fit: BoxFit.fill,
        ),
      ),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              width: mediaQueryData.size.width,
              height: mediaQueryData.size.height,
              child: PageView(
                controller: _pageController,
                physics: AlwaysScrollableScrollPhysics(),
                children: <Widget>[
                  lesson1 != null
                      ? Ch3Pages(
                          fromLesson: '1',
                          image1: 'assets/images/taween_fath_hand.png',
                          image2: 'assets/images/icon_fat_ja.png',
                          image3: 'assets/images/tanween_alfath.png',
                          lesson: lesson1,
                          onTap: (value) {
                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                  lesson2 != null
                      ? Ch3Pages(
                          image1: 'assets/images/tanween_kasr.png',
                          image2: 'assets/images/object_2.png',
                          image3: 'assets/images/tanween_kasr2.png',
                          fromLesson: '2',
                          lesson: lesson2,
                          onTap: (value) {
                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                  lesson3 != null
                      ? Ch3Pages(
                          image1: 'assets/images/tanween_dom.png',
                          image3: 'assets/images/tanween_dom2.png',
                          fromLesson: '3',
                          lesson: lesson3,
                          onTap: (value) {
                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                ],
                onPageChanged: (currentPage) {
                  page = currentPage;
                  currentLesson = "${page + 1}";
                  audioCache.fixedPlayer.stop();
                  refresh();
                },
              ),
            ),
          ),
          /*Container(
            width: 250,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    InkWell(
                      onTap: () {},
                      child: CircleAvatar(
                        backgroundColor: ThemeColors.colorLightBrown,
                        radius: 12,
                        child: Image.asset(
                          "assets/icons/ic_1.png",
                          height: 12,
                          width: 12,
                          color: ThemeColors.colorBrown,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      child: CircleAvatar(
                        backgroundColor: ThemeColors.colorLightBrown,
                        radius: 15,
                        child: Image.asset(
                          "assets/icons/ic_2.png",
                          height: 12,
                          width: 12,
                          color: ThemeColors.colorBrown,
                        ),
                      ),
                    ),
                    StreamBuilder(
                      stream: audioCache.fixedPlayer.onPlayerStateChanged,
                      initialData: false,
                      builder: (context, currentPlayerState) {
                        print(
                            'current player state ===>> ${currentPlayerState.data}');

                        return InkWell(
                          onTap: () {},
                          child: CircleAvatar(
                            backgroundColor: ThemeColors.colorLightBrown,
                            radius: 20,
                            child: Image.asset(
                              currentPlayerState.data ==
                                      AudioPlayerState.PLAYING
                                  ? "assets/icons/ic_pause.png"
                                  : "assets/icons/ic_3.png",
                              height: 20,
                              width: 18,
                              color: ThemeColors.colorBrown,
                            ),
                          ),
                        );
                      },
                    ),
                    InkWell(
                      onTap: () {},
                      child: CircleAvatar(
                        backgroundColor: ThemeColors.colorLightBrown,
                        radius: 15,
                        child: Image.asset(
                          "assets/icons/ic_4.png",
                          height: 12,
                          width: 12,
                          color: ThemeColors.colorBrown,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      child: CircleAvatar(
                        backgroundColor: ThemeColors.colorLightBrown,
                        radius: 12,
                        child: Image.asset(
                          "assets/icons/ic_5.png",
                          height: 12,
                          width: 12,
                          color: ThemeColors.colorBrown,
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  width: 150,
                  margin: EdgeInsets.only(top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      InkWell(
                        onTap: () {},
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius: 15,
                          child: Image.asset(
                            "assets/icons/ic_8.png",
                            height: 20,
                            width: 20,
                            color: ThemeColors.colorBrown,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {},
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius: 15,
                          child: Image.asset(
                            "assets/icons/ic_6.png",
                            height: 20,
                            width: 20,
                            color: ThemeColors.colorBrown,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {},
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius: 15,
                          child: Image.asset(
                            "assets/icons/ic_7.png",
                            height: 20,
                            width: 20,
                            color: ThemeColors.colorBrown,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 250,
                  height: 35,
                  child: StreamBuilder(
                    stream: audioCache.fixedPlayer.onAudioPositionChanged,
                    initialData: const Duration(),
                    builder: (context, snapshot) {
//                  if (snapshot.hasData) {
////                    print(
////                        'duration string ===>> ${durationToString(snapshot.data)}');
//                    print(
//                        'duration total ===>> ${durationToInt(snapshot.data)}');
//                  }

//                  SliderTheme(
//                      data: SliderTheme.of(context).copyWith(
//                        activeTrackColor: ThemeColors.colorBrown,
//                        inactiveTrackColor: ThemeColors.colorBrown,
//                        trackShape: RectangularSliderTrackShape(),
//                        trackHeight: 2.0,
//                        thumbColor: ThemeColors.colorBrown,
//                        thumbShape:
//                        RoundSliderThumbShape(enabledThumbRadius: 8.0),
//                        overlayColor: ThemeColors.colorBrown.withAlpha(32),
//                        overlayShape:
//                        RoundSliderOverlayShape(overlayRadius: 28.0),
//                      ),
                      currentDuration = durationToDouble(snapshot.data);

                      print('current position ===>> $currentDuration');

//                        if (currentTotalDuration == currentDuration) {
//                          currentDuration = 0.0;
////                          refresh();
//                        }

                      return Slider(
                        inactiveColor: ThemeColors.colorBrown,
                        activeColor: ThemeColors.colorBrown,
                        min: 0.0,
                        max: currentTotalDuration,
                        value: currentDuration,
                        onChanged: (value) {
                          audioCache.fixedPlayer
                              .seek(Duration(seconds: value.toInt()));
                        },
                        onChangeEnd: (value) {},
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
         page != 0
              ? CommonBackButton(
                  onTap: () {
                    _pageController.animateToPage(page--,
                        duration: Duration(milliseconds: 400),
                        curve: Curves.linear);
                  },
                )
              : SizedBox(),
          SizedBox(height: 20)*/
        ],
      ),
    );
  }

  getHeaderImages() {
    return Container(
      height: 50,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Image.asset(
            'assets/images/bg_1.png',
            width: 40,
            height: 30,
            fit: BoxFit.cover,
          ),
          Image.asset(
            'assets/images/bg_1.png',
            width: 40,
            height: 30,
            fit: BoxFit.cover,
          ),
          Image.asset(
            'assets/images/bg_1.png',
            width: 40,
            height: 30,
            fit: BoxFit.cover,
          ),
        ],
      ),
    );
  }

  String durationToString(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes =
        twoDigits(duration.inMinutes.remainder(Duration.minutesPerHour));
    String twoDigitSeconds =
        twoDigits(duration.inSeconds.remainder(Duration.secondsPerMinute));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  double durationToDouble(Duration duration) {
    var twoDigitMinutes = duration.inMinutes.remainder(Duration.minutesPerHour);
    var twoDigitSeconds =
        duration.inSeconds.remainder(Duration.secondsPerMinute);

    int totalDuration = (twoDigitMinutes * 60) + twoDigitSeconds;
    return double.parse(totalDuration.toString());
  }

  loader() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: 2, gravity: Toast.BOTTOM);
  }
}
