import 'package:ekraa_app/model/chapter/chapter2/chapter2Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch3Pages extends StatefulWidget {
  final LessonsAll lesson;
  final HorofClickCallback onTap;
  final String image1;
  final String image2;
  final String image3;
  final String fromLesson;

  const Ch3Pages({
    Key key,
    this.lesson,
    this.onTap,
    this.image1 = "",
    this.image2 = "",
    this.image3 = "",
    this.fromLesson,
  }) : super(key: key);

  @override
  _Ch3PagesState createState() => _Ch3PagesState();
}

class _Ch3PagesState extends State<Ch3Pages> {
  String s1;

  @override
  void initState() {
    // widget.fromLesson will return 1 or 2 or 3 now this will be very essential for us to know which title and data and list belongs to which lesson
    // since we are managing 3 lessons in one page because
    // it has similar UI's so to duplicate code we are managing 2 lessons in one page by just passing one flag that is == widget.fromLesson
    switch (widget.fromLesson) {
      case "1":
        s1 = "Fat-ha";
        break;
      case "2":
        s1 = "Kasra";
        break;
      case "3":
        s1 = "Dumma";
        break;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: kOrientation
            ? heightWords * 4 + heightTable * 12
            : heightWords * 6 + heightTable * 15,
        child: Column(
          children: <Widget>[
            SizedBox(height: 30),
            getHeaderImages(),
            SizedBox(height: 10),
            getWordsLayout(),
            SizedBox(height: 10),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 15, left: 10, right: 10),
                child: getTableLayout(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getHeaderImages() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 10),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          getImage(widget.image1),
          Column(
            children: <Widget>[
              Text(
                widget.lesson.subTitle,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ThemeColors.colorRose,
                  fontFamily: 'Uthmanic',
                  fontSize: kDeviceType == DeviceScreenType.Mobile
                      ? textSize + 3
                      : textSize + 6,
                  fontWeight: FontWeight.w900,
                ),
              ),
              Text(
                s1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ThemeColors.colorBorderCoffee,
                  fontFamily: 'Uthmanic',
                  fontSize: textSize - 2,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ],
          ),
          getImage(widget.image3)
        ],
      ),
    );
  }

  getImage(String imageUrl) {
    return Image.asset(
      imageUrl,
      width: kDeviceType == DeviceScreenType.Mobile ? 60 : 110,
      height: kDeviceType == DeviceScreenType.Mobile ? 60 : 110,
      fit: BoxFit.fitWidth,
    );
  }

  getWordsLayout() {
    return Column(
      children: <Widget>[
        // this will return list of words between given range as we already knew we want this amount of words in one row same procedure repeats for rest of the lists
        // each list represents one row of the words layout
        getRow(widget.lesson.filler.getRange(0, 7).toList()),
        getRow(widget.lesson.filler.getRange(7, 14).toList()),
        getRow(widget.lesson.filler.getRange(14, 21).toList()),
        getRow(widget.lesson.filler.getRange(21, 28).toList())
      ],
    );
  }

  getRow(List<Filler> row) {
    return Container(
      alignment: Alignment.center,
      height: kOrientation ? heightWords + 15 : heightWords * 2,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: row.length,
        reverse: true,
        itemBuilder: (context, index) {
          return itemWord(row[index]);
        },
      ),
    );
  }

  getTableLayout() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(color: Colors.black),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // this will return list of table words between given range as we already knew from which range table is starting and on which on value its ending
            // and same procedure repeats for rest of the lists
            // each list represents one row of the table
            getTableRow(widget.lesson.filler.getRange(28, 33).toList()),
            getTableRow(widget.lesson.filler.getRange(33, 38).toList()),
            getTableRow(widget.lesson.filler.getRange(38, 43).toList()),
            getTableRow(widget.lesson.filler.getRange(43, 48).toList()),
            getTableRow(widget.lesson.filler.getRange(48, 53).toList()),
            getTableRow(widget.lesson.filler.getRange(53, 58).toList()),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getTableRow(List<Filler> row) {
    return Container(
      height: kOrientation ? heightTable : heightTable + 15,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(color: Colors.black),
          right: BorderSide(color: Colors.black),
        ),
      ),
      child: ListView.builder(
        itemCount: row.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(row[index]);
        },
      ),
    );
  }

  itemWord(Filler horof) {
    return GestureDetector(
      onTap: () {
        widget.onTap(horof);
      },
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 1.5, vertical: 1.5),
            width: kOrientation ? heightWords + 4.5 : heightWords * 1.8,
            height: kOrientation ? heightWords + 4.5 : heightWords * 1.8,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/shape.png'),
                fit: BoxFit.fill,
              ),
//            border: Border.all(color: ThemeColors.colorBrown),
//            borderRadius: BorderRadius.circular(3),
            ),
            child: Center(
              child: Text(
                horof.alpha.trim(),
                textAlign: TextAlign.center,
                textDirection: TextDirection.rtl,
                style: TextStyle(
                  color: Colors.blue[800],
                  fontFamily: 'Uthmanic',
                  fontSize: textSize,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  itemTable(Filler horof) {
    return InkWell(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.only(top: 3),
        width: MediaQuery.of(context).size.width / 5 - 8,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(width: 1, color: Colors.black),
            bottom: BorderSide(color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black87,
              fontFamily: 'Uthmanic',
              fontSize: kOrientation ? textSize - 1 : textSize,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
