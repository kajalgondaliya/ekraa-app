import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch4Page1 extends StatefulWidget {
  final Chapter6Model model1;
  final HorofClickCallback onTap;
  final fromLesson;
  final image1;
  final image2;
  final image3;
  final image4;

  const Ch4Page1(
      {Key key,
      this.model1,
      this.onTap,
      this.fromLesson,
      this.image1,
      this.image2,
      this.image3,
      this.image4})
      : super(key: key);

  @override
  _Ch4Page1State createState() => _Ch4Page1State();
}

class _Ch4Page1State extends State<Ch4Page1> {
  @override
  void initState() {
    // widget.fromLesson will return 1 or 2 now this will be very essential for us to know which title and data and list belongs to which lesson
    // since we are managing 2 lessons in one page because
    // it has similar UI's so to duplicate code we are managing 2 lessons in one page by just passing one flag that is == widget.fromLesson
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 20),
            widget.fromLesson == "1"
                ? getHeadersLesson1('1')
                : getHeadersLesson2('1'),
            SizedBox(height: 10),
            getTable(from: "table1"),
            SizedBox(height: 20),
            widget.fromLesson == "1"
                ? getHeadersLesson1('2')
                : getHeadersLesson2('2'),
            SizedBox(height: 20),
            getTable(from: "table2"),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getHeadersLesson1(String fromTable) {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                fromTable == "1" ? widget.model1.title : widget.model1.subTitle,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ThemeColors.colorRose,
                  fontFamily: 'Uthmanic',
                  fontSize: textSize + 5,
                  fontWeight: FontWeight.w900,
                ),
              ),
              Text(
                fromTable == "1"
                    ? "The Long Vowel Alif"
                    : 'Alif above another letter',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ThemeColors.colorBorderCoffee,
                  fontFamily: 'Uthmanic',
                  fontSize: textSize - 2,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ],
          ),
          fromTable == "1"
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: kOrientation ? 5 : 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            'بَـٰ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.red,
                              fontFamily: 'Uthmanic',
                              fontSize: textSize,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          SizedBox(width: 10),
                          Text(
                            'بَـا',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.red,
                              fontFamily: 'Uthmanic',
                              fontSize: textSize + 4,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      transform: Matrix4.translationValues(0.0, -20.0, 0.0),
                      child: Text(
                        'ـــــــــــ',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: ThemeColors.colorBorderCoffee,
                          fontFamily: 'Uthmanic',
                          fontSize: textSize,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ],
                )
              : SizedBox(),
        ],
      ),
    );
  }

  getHeadersLesson2(String fromTable) {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Column(
            children: <Widget>[
              Text(
                fromTable == "1" ? 'ياء المد' : 'واو المد',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ThemeColors.colorRose,
                  fontFamily: 'Uthmanic',
                  fontSize: textSize + 5,
                  fontWeight: FontWeight.w900,
                ),
              ),
              Text(
                fromTable == "1" ? "The Long Vowel:Ya'a" : "The Long Vowel Wow",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ThemeColors.colorBorderCoffee,
                  fontFamily: 'Uthmanic',
                  fontSize: textSize - 2,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    fromTable == "1" ? 'ۧ' : "و ُ",
                    textAlign: TextAlign.end,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      color: Colors.red,
                      fontFamily: 'Uthmanic',
                      fontSize: fromTable == "1" ? textSize + 6 : textSize,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                  SizedBox(width: 10),
                  fromTable == "1"
                      ? Padding(
                          padding: EdgeInsets.only(right: 15),
                          child: Text(
                            'ى',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.red,
                              fontFamily: 'Uthmanic',
                              fontSize: textSize + 4,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                        )
                      : SizedBox(),
                ],
              ),
              SizedBox(width: 10),
              Container(
                transform: Matrix4.translationValues(0.0, -15.0, 0.0),
                child: Text(
                  fromTable == "1" ? 'ـــــــــ' : 'ـــــ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: ThemeColors.colorBorderCoffee,
                    fontFamily: 'Uthmanic',
                    fontSize: textSize,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  getTable({String from}) {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(width: 1, color: Colors.black),
          left: BorderSide(width: 1, color: Colors.black),
          right: BorderSide(width: 1, color: Colors.black),
        ),
      ),
      child: Column(
        children: <Widget>[
          if (from == "table1") getTable1() else getTable2(),
        ],
      ),
    );
  }

  getTable1() {
    return Column(
      children: <Widget>[
        getRow(widget.model1.filler.getRange(0, 5).toList()),
        getRow(widget.model1.filler.getRange(5, 10).toList()),
        getRow(widget.model1.filler.getRange(10, 15).toList()),
        getRow(widget.model1.filler.getRange(15, 20).toList()),
        getRow(widget.model1.filler.getRange(20, 25).toList()),
        getRow(widget.model1.filler.getRange(25, 30).toList()),
      ],
    );
  }

  getTable2() {
    return Column(
      children: <Widget>[
        getRow(widget.model1.filler.getRange(30, 35).toList()),
        getRow(widget.model1.filler.getRange(35, 40).toList()),
        getRow(widget.model1.filler.getRange(40, 45).toList()),
        getRow(widget.model1.filler.getRange(45, 50).toList()),
        getRow(widget.model1.filler.getRange(50, 55).toList()),
        getRow(widget.model1.filler.getRange(55, 60).toList()),
      ],
    );
  }

  getRow(List<dynamic> filler) {
    return Container(
      height: kOrientation ? heightTable : heightTable + 10,
      child: ListView.builder(
        itemCount: filler.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(filler[index], filler.length, index);
        },
      ),
    );
  }

  itemTable(Filler horof, int size, int index) {
//    print('index ===>>> $index');

    return InkWell(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.only(top: 3, left: 3, right: 3),
        width: (MediaQuery.of(context).size.width - 40) / size,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(width: 1, color: Colors.black),
            bottom: BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
