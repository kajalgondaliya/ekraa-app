import 'dart:convert';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/ui/base_layout/base_home_layout.dart';
import 'package:ekraa_app/ui/chapter4/ch_4_lesson_1_2.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:toast/toast.dart';

class Chapter4 extends StatefulWidget {
  @override
  _Chapter4State createState() => _Chapter4State();
}

class _Chapter4State extends State<Chapter4> {
  var _pageController = PageController();
  int page = 0;

  var currentLesson = "1";
  double currentTotalDuration = 0.0;
  double currentDuration = 0.0;

  List<dynamic> allLessons;

  List<Chapter6Model> lesson1List = [];
  List<Chapter6Model> lesson2List = [];

  AudioCache audioCache = AudioCache();

  bool isCompleted = false;

  String toastMsg = 'Audio is not avaiable.';

  refresh() {
    SchedulerBinding.instance.addPostFrameCallback((callback) {
      if (mounted) setState(() {});
    });
  }

  @override
  void initState() {
//    _assetsAudioPlayer.playlistAudioFinished.listen((data) {
//      print("finished : $data");
//    });

    audioCache.fixedPlayer = AudioPlayer();
    audioCache.fixedPlayer.onPlayerCompletion.listen((event) {
      print('complete event called ==>>');
      isCompleted = true;
      refresh();
    });
    audioCache.fixedPlayer.onDurationChanged.listen((data) {
//      print("current : ${data.duration}.");
//      print("current : ${data.duration}.");
      currentTotalDuration = durationToDouble(data);
//      print('currentTotalDuration : ${durationToDouble(data)}');
    });

    loadJsonFile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // This function will set parameters device width,height,device_type,current_orientation,dynamic {textSize,heightTable,heightWords} according to screen size.
    // to know which variable holds which value please go to function declaration.
    // this is very essential as we are calculating above values dynamically according to device type and device orientation.
    setDeviceLayoutSizes(context);
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        body: BaseLayout(
          menuIcon: Icons.arrow_back,
          title1: 'Chapter',
          title2: ' 4',
          onTapHome: () {
            stopMusicPlayerAndGoBack();
          },
          onTapMenu: () {
            stopMusicPlayerAndGoBack();
          },
          child: getMainLayout(MediaQuery.of(context)),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() async {
    audioCache.fixedPlayer.stop();
    return true; // return true if the route to be popped
  }

  stopMusicPlayerAndGoBack() {
    audioCache.fixedPlayer.stop();
    Navigator.of(context).pop();
  }

  loadJsonFile() async {
    await rootBundle.loadString('assets/json/chapter4.json').then((onValue) {
      allLessons = jsonDecode(onValue); // all lessons list of chapter 6

      lesson1List.add(Chapter6Model.fromJson(allLessons[0]));
      lesson2List.add(Chapter6Model.fromJson(allLessons[1]));

      refresh();
    });
  }

  getMainLayout(MediaQueryData mediaQueryData) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/white_page.jpg'),
          fit: BoxFit.fill,
        ),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
//            Padding(
//              padding: EdgeInsets.all(10.0),
//              child: RichText(
//                text: TextSpan(
//                  children: <TextSpan>[
//                    TextSpan(
//                      text: 'Chapter ',
//                      style: TextStyle(
//                        color: ThemeColors.colorBrown,
//                        fontFamily: 'Uthmanic',
//                        fontSize: 18,
//                        fontWeight: FontWeight.normal,
//                      ),
//                    ),
//                    TextSpan(
//                      text: '4',
//                      style: TextStyle(
//                        color: ThemeColors.colorBrown,
//                        fontFamily: 'Uthmanic',
//                        fontSize: 24,
//                        fontWeight: FontWeight.normal,
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
            Expanded(
              child: PageView(
                controller: _pageController,
                physics: AlwaysScrollableScrollPhysics(),
                children: <Widget>[
                  lesson1List != null && lesson1List.isNotEmpty
                      ? Ch4Page1(
                          model1: lesson1List[0],
                          fromLesson: '1',
                          image1: 'assets/images/object_1.png',
                          image2: 'assets/images/object_2.png',
                          image3: 'assets/images/object_3.png',
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                  lesson2List != null && lesson2List.isNotEmpty
                      ? Ch4Page1(
                          model1: lesson2List[0],
                          fromLesson: '2',
                          image1: 'assets/images/object_1.png',
                          image2: 'assets/images/object_2.png',
                          image3: 'assets/images/object_3.png',
                          image4: 'assets/images/object_3.png',
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                ],
                onPageChanged: (currentPage) {
                  page = currentPage;
                  currentLesson = "${page + 1}";
                  audioCache.fixedPlayer.stop();
                  refresh();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  String durationToString(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes =
        twoDigits(duration.inMinutes.remainder(Duration.minutesPerHour));
    String twoDigitSeconds =
        twoDigits(duration.inSeconds.remainder(Duration.secondsPerMinute));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  double durationToDouble(Duration duration) {
    var twoDigitMinutes = duration.inMinutes.remainder(Duration.minutesPerHour);
    var twoDigitSeconds =
        duration.inSeconds.remainder(Duration.secondsPerMinute);

    int totalDuration = (twoDigitMinutes * 60) + twoDigitSeconds;
    return double.parse(totalDuration.toString());
  }

  loader() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: 2, gravity: Toast.BOTTOM);
  }
}
