import 'package:ekraa_app/theme/colors.dart';
import 'package:flutter/material.dart';

class BaseLayout extends StatefulWidget {
  final Widget child;
  final IconData menuIcon;
  final onTapMenu;
  final onTapHome;
  final title1;
  final title2;

  const BaseLayout(
      {Key key,
      this.title1,
      this.title2,
      this.menuIcon,
      this.child,
      this.onTapMenu,
      this.onTapHome})
      : super(key: key);

  @override
  _BaseLayoutState createState() => _BaseLayoutState();
}

class _BaseLayoutState extends State<BaseLayout> {
  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;
    var mediaData = MediaQuery.of(context);
    return Container(
      width: mediaData.size.width,
      height: mediaData.size.height,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              getAppBar(statusBarHeight),
              Expanded(
                child: Container(
                  width: mediaData.size.width,
                  height: mediaData.size.height,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/white_page.jpg'),
                      fit: BoxFit.fill,
                    ),
                  ),
                  child: widget.child,
                ),
              ),
            ],
          ),
          Positioned(
            right: 30,
            top: 65,
            child: InkWell(
              onTap: widget.onTapHome,
              child: Icon(
                Icons.home,
                size: 40,
                color: ThemeColors.colorBrown,
              ),
            ),
          )
        ],
      ),
    );
  }

  getAppBar(statusBarHeight) {
    return Container(
      height: 90,
      padding: EdgeInsets.only(top: statusBarHeight),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_1.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Stack(
        children: [
          IconButton(
            iconSize: 30,
            padding: EdgeInsets.only(top: 15),
            onPressed: widget.onTapMenu,
            icon: Icon(
              widget.menuIcon,
              color: ThemeColors.colorBrown,
            ),
          ),
          Center(
            child: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: widget.title1,
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Uthmanic',
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  TextSpan(
                    text: widget.title2,
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Uthmanic',
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
