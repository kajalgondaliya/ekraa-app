import 'package:ekraa_app/files/chapters_files.dart';
import 'package:ekraa_app/model/chapter/chapters.dart';
import 'package:ekraa_app/sizing_info/orientation_layout.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/ui/base_layout/base_home_layout.dart';
import 'package:ekraa_app/utils/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:polygon_clipper/polygon_border.dart';
import 'package:polygon_clipper/polygon_clipper.dart';
import 'package:url_launcher/url_launcher.dart';

class SelectChapter extends StatefulWidget {
  static const id = 'Lessons';

  @override
  _SelectChapterState createState() => _SelectChapterState();
}

class _SelectChapterState extends State<SelectChapter> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: ThemeColors.colorLightBrown.withOpacity(
              .2), //or any other color you want. e.g Colors.blue.withOpacity(0.5)
        ),
        child: SizedBox(
          width: 150,
          child: Drawer(
            elevation: 20.0,
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                SizedBox(height: 150),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    IconButton(
                      iconSize: 80,
                      color: ThemeColors.colorLightCoffee,
                      icon: FaIcon(FontAwesomeIcons.book),
                      onPressed: () {
                        openurl('https://minhaj.ca/collections/all');
                      },
                    ),
                    Text(
                      'Our Products',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: ThemeColors.colorLightCoffee,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 15.0),
                      child: IconButton(
                        iconSize: 80,
                        color: ThemeColors.colorLightCoffee,
                        icon: FaIcon(
                          FontAwesomeIcons.users,
                        ),
                        onPressed: () {
                          openurl('https://ikraa.ca/Ekraa/about');
                        },
                      ),
                    ),
                    Text(
                      'ABOUT US',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: ThemeColors.colorLightCoffee,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    IconButton(
                      iconSize: 80,
                      color: ThemeColors.colorLightCoffee,
                      icon: FaIcon(FontAwesomeIcons.commentDots),
                      onPressed: () {
                        openurl('https://ikraa.ca/Ekraa/contact');
                      },
                    ),
                    Text(
                      'Contact Us',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: ThemeColors.colorLightCoffee,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      body: BaseLayout(
        menuIcon: Icons.menu,
        title1: 'Lessons',
        title2: ' Map',
        onTapHome: () {
//          Navigator.pushNamed(context, SelectChapter.id);
        },
        onTapMenu: () {
          _scaffoldKey.currentState.openDrawer();
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  child: OrientationLayout(
                    portrait: GridView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.only(top: 20, bottom: 20),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 20,
                        childAspectRatio: MediaQuery.of(context).size.width /
                            (MediaQuery.of(context).size.height / 2.5),
                      ),
                      itemCount: allChapters.length,
                      itemBuilder: (context, index) {
                        return itemChapter(context, allChapters[index]);
                      },
                    ),
                    landscape: GridView.builder(
                      padding: EdgeInsets.only(top: 20, bottom: 20),
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 20,
                        childAspectRatio: MediaQuery.of(context).size.height /
                            (MediaQuery.of(context).size.width / 3),
                      ),
                      itemCount: allChapters.length,
                      itemBuilder: (context, index) {
                        return itemChapter(context, allChapters[index]);
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  itemChapter(BuildContext context, Chapter chapter) {
    return InkWell(
      onTap: () {
        switch (chapter.number) {
          case "1":
            navigateToSubPage(context, AppRoutes.chapter1);
            break;
          case "2":
            navigateToSubPage(context, AppRoutes.chapter2);
            break;
          case "3":
            navigateToSubPage(context, AppRoutes.chapter3);
            break;
          case "4":
            navigateToSubPage(context, AppRoutes.chapter4);
            break;
          case "5":
            navigateToSubPage(context, AppRoutes.chapter5);
            break;
          case "6":
            navigateToSubPage(context, AppRoutes.chapter6);
            break;
        }
      },
      child: Container(
        child: ClipPolygon(
          sides: 6,
          child: Container(
            decoration: ShapeDecoration(
              color: ThemeColors.colorCoffee,
              shape: PolygonBorder(
                sides: 6,
                border: BorderSide(
                  color: ThemeColors.colorBorderCoffee,
                  width: 3,
                ),
              ),
            ),
            child: Center(
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: '${chapter.number}\n',
                      style: TextStyle(
                        color: ThemeColors.colorLightCoffee,
                        fontSize: 44,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(
                      text: '${chapter.name}',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
//        boxShadows: [
//          PolygonBoxShadow(color: Colors.black, elevation: 5.0),
//        ],
//        sides: 6,
//        borderRadius: 5.0,
        ),
      ),
    );
  }

  Future navigateToSubPage(context, String chapter) async {
    Navigator.pushNamed(context, chapter);
  }

  void openurl(var url) {
    launch(url);
  }
}
