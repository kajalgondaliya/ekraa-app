import 'package:ekraa_app/model/chapter/chapter2/chapter2Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch1Page1 extends StatefulWidget {
  final LessonsAll lesson;
  final HorofClickCallback onTap;
  final String image1;
  final String image2;
  final String image3;

  const Ch1Page1(
      {Key key, this.lesson, this.onTap, this.image1, this.image2, this.image3})
      : super(key: key);

  @override
  _Ch1Page1State createState() => _Ch1Page1State();
}

class _Ch1Page1State extends State<Ch1Page1> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 30),
          getHeaderImages(),
          SizedBox(height: 20),
          getWordsLayout(),
        ],
      ),
    );
  }

  getHeaderImages() {
    return Container(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
//          getImage(widget.image1),
          Column(
            children: <Widget>[
              Text(
                widget.lesson.subTitle,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ThemeColors.colorRose,
                  fontFamily: 'Uthmanic',
                  fontSize: textSize,
                  fontWeight: FontWeight.w900,
                ),
              ),
              Text(
                "Arabic Alphabet",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ThemeColors.colorBorderCoffee,
                  fontFamily: 'Uthmanic',
                  fontSize: textSize - 2,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ],
          ),
//          getImage(widget.image3),
        ],
      ),
    );
  }

  getImage(String imageUrl) {
    double size = kDeviceType == DeviceScreenType.Mobile ? 60 : 110;

    return Image.asset(
      imageUrl,
      width: size,
      height: size,
      fit: BoxFit.fitWidth,
    );
  }

  getWordsLayout() {
    return Column(
      children: <Widget>[
        SizedBox(height: 10),
        // this will return list of words between given range as we already knew we want this amount of words in one row same procedure repeats for rest of the lists
        // each list represents one row of the words layout
        getRow(widget.lesson.filler.getRange(0, 5).toList()),
        getRow(widget.lesson.filler.getRange(5, 10).toList()),
        getRow(widget.lesson.filler.getRange(10, 15).toList()),
        getRow(widget.lesson.filler.getRange(15, 20).toList()),
        getRow(widget.lesson.filler.getRange(20, 25).toList()),
        getRow(widget.lesson.filler.getRange(25, 29).toList()),
      ],
    );
  }

  getRow(List<Filler> row) {
    return Container(
      alignment: Alignment.center,
      height: kOrientation ? heightWords + 30 : heightWords * 1.8,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: row.length,
        reverse: true,
        itemBuilder: (context, index) {
          return itemWord(row[index]);
        },
      ),
    );
  }

  itemWord(Filler horof) {
    return GestureDetector(
      onTap: () {
        widget.onTap(horof);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 1.5, vertical: 1.5),
            padding: EdgeInsets.all(3),
            width: heightWords * 1.5,
            height: heightWords * 1.5,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/shape.png'),
                fit: BoxFit.fill,
              ),
//            border: Border.all(color: ThemeColors.colorBrown),
//            borderRadius: BorderRadius.circular(3),
            ),
            child: Center(
              child: Text(
                horof.alpha,
                style: TextStyle(
                  color: Colors.blue[800],
                  fontFamily: 'Uthmanic',
                  fontSize: textSize + 5,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
