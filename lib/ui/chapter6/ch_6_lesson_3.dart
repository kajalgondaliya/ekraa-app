//import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
//import 'package:ekraa_app/theme/colors.dart';
//import 'package:flutter/material.dart';
//
//import 'chapter_6_main.dart';
//import 'package:ekraa_app/ui/select_chapters/select_chapter.dart';
//import 'package:ekraa_app/utils/ui_utils.dart';
//typedef HorofClickCallback = Function(Filler horof);
//
//class Ch6Page3 extends StatefulWidget {
//  final Chapter6Model model1;
//  final Chapter6Model model2;
//  final Chapter6Model model3;
//  final Chapter6Model model4;
//  final HorofClickCallback onTap;
//
//  const Ch6Page3(
//      {Key key, this.model1, this.model2, this.model3, this.model4, this.onTap})
//      : super(key: key);
//
//  @override
//  _Ch6Page3State createState() => _Ch6Page3State();
//}
//
//class _Ch6Page3State extends State<Ch6Page3> {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      padding: EdgeInsets.symmetric(horizontal: 20),
//      child: SingleChildScrollView(
//        child: Column(
//          children: <Widget>[
//            Padding(
//              padding: const EdgeInsets.all(10.0),
//              child: Text(
//                widget.model1.title,
//                textDirection: TextDirection.rtl,
//                style: TextStyle(
//                  color: ThemeColors.colorCoffee2,
//                  fontFamily: 'Uthmanic',
//                  fontSize: textSize + 5,
//                  fontWeight: FontWeight.bold,
//                ),
//              ),
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceAround,
//              children: <Widget>[
//                Text(
//                  "Lesson one",
//                  style: TextStyle(
//                    color: Colors.black,
//                    fontFamily: 'Uthmanic',
//                    fontSize: kOrientation ? 14 : 16,
//                    fontWeight: FontWeight.bold,
//                  ),
//                ),
//                Text(
//                  'الدرس الأول :',
//                  textDirection: TextDirection.rtl,
//                  style: TextStyle(
//                    color: ThemeColors.colorCoffee2,
//                    fontFamily: 'Uthmanic',
//                    fontSize: kOrientation ? 16 : 20,
//                    fontWeight: FontWeight.bold,
//                  ),
//                ),
//              ],
//            ),
//            SizedBox(height: 10),
//            Text(
//              '* همزة الوصل إذا جاء بعدها همزة قطع ساكنة، تتحول الهمزة إلى حرف مد موافق لحركة همزة الوصل (وهذا في حالة البدء)',
//              textDirection: TextDirection.rtl,
//              textAlign: TextAlign.center,
//              style: TextStyle(
//                color: Colors.red,
//                fontFamily: 'Uthmanic',
//                fontSize: kOrientation ? 16 : 20,
//                fontWeight: FontWeight.bold,
//              ),
//            ),
//            SizedBox(height: 10),
//            Text(
//              "*hamzatulwasl followed by Hamzatulqataa with sukoon; the second hamza will turn into long vowel sound for hamzat alwasl and the variation will follow the rule of hamzat alwasl upon starting the recitation.",
//              textAlign: TextAlign.center,
//              textDirection: TextDirection.rtl,
//              style: TextStyle(
//                color: Colors.black,
//                fontFamily: 'Uthmanic',
//                fontSize: kOrientation ? 14 : 16,
//                fontWeight: FontWeight.bold,
//              ),
//            ),
//            SizedBox(height: 20),
//            getTable1(widget.model2.filler),
//            SizedBox(height: 20),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceAround,
//              children: <Widget>[
//                Text(
//                  "Lesson two",
//                  style: TextStyle(
//                    color: Colors.black,
//                    fontFamily: 'Uthmanic',
//                    fontSize: kOrientation ? 14 : 16,
//                    fontWeight: FontWeight.bold,
//                  ),
//                ),
//                Text(
//                  'الدرس الثاني',
//                  textDirection: TextDirection.rtl,
//                  style: TextStyle(
//                    color: ThemeColors.colorCoffee2,
//                    fontFamily: 'Uthmanic',
//                    fontSize: kOrientation ? 16 : 20,
//                    fontWeight: FontWeight.bold,
//                  ),
//                ),
//              ],
//            ),
//            SizedBox(height: 20),
//            getTable1(widget.model3.filler),
//            SizedBox(height: 20),
//            Text(
//              'مع أن الثالث مضموم لكن سنبدأ بالكسر.. لأن الضمة ليست أصلية',
//              textDirection: TextDirection.rtl,
//              textAlign: TextAlign.center,
//              style: TextStyle(
//                color: Colors.red,
//                fontFamily: 'Uthmanic',
//                fontSize: kOrientation ? 16 : 20,
//                fontWeight: FontWeight.bold,
//              ),
//            ),
//            SizedBox(height: 20),
//            Text(
//              'Although the third letter carries Damma, we are going to pronounce hamzat al wasl as Kasra because the short vowel here is not the original sound',
//              textAlign: TextAlign.center,
//              style: TextStyle(
//                color: Colors.black,
//                fontFamily: 'Uthmanic',
//                fontSize: kOrientation ? 14 : 16,
//                fontWeight: FontWeight.bold,
//              ),
//            ),
//            SizedBox(height: 20),
//            getTable3(),
//            SizedBox(height: 30),
//          ],
//        ),
//      ),
//    );
//  }
//
//  getTable1(List row) {
//    return Container(
//      width: MediaQuery.of(context).size.width - 40,
//      height: kOrientation ? heightTable : heightTable + 10,
//      decoration: BoxDecoration(
////        color: ThemeColors.colorGreyLight,
//        border: Border(
//          top: BorderSide(color: Colors.black),
//          left: BorderSide(color: Colors.black),
//          right: BorderSide(color: Colors.black),
//        ),
//      ),
//      child: getRow(row, '1'),
//    );
//  }
//
//  getTable3() {
//    return Container(
//      width: MediaQuery.of(context).size.width - 40,
//      decoration: BoxDecoration(
////        color: ThemeColors.colorGreyLight,
//        border: Border(
//          top: BorderSide(color: Colors.black),
//          left: BorderSide(color: Colors.black),
//          right: BorderSide(color: Colors.black),
//        ),
//      ),
//      child: Column(
//        children: <Widget>[
//          getRow(widget.model4.filler.getRange(0, 6).toList(), '2'),
//          getRow(widget.model4.filler.getRange(6, 12).toList(), '2'),
//        ],
//      ),
//    );
//  }
//
//  getRow(List<dynamic> row, String from) {
//    return Container(
//      height: from == "1" ? heightTable : heightTable + 20,
//      child: ListView.builder(
//        itemCount: row.length,
//        physics: NeverScrollableScrollPhysics(),
//        scrollDirection: Axis.horizontal,
//        reverse: true,
//        itemBuilder: (context, index) {
//          return itemTable(row[index], row.length, index, from);
//        },
//      ),
//    );
//  }
//
//  itemTable(Filler horof, int size, int index, String from) {
//    return InkWell(
//      onTap: () {
//        widget.onTap(horof);
//      },
//      child: Container(
//        padding: EdgeInsets.only(top: 3),
//        width: (MediaQuery.of(context).size.width - 40) / size,
//        alignment: Alignment.center,
//        decoration: BoxDecoration(
//          border: Border(
//            left: BorderSide(width: 1, color: Colors.black),
//            bottom: BorderSide(width: 1, color: Colors.black),
//          ),
//        ),
//        child: Center(
//          child: Text(
//            horof.word,
//            textAlign: TextAlign.center,
//            textDirection: TextDirection.rtl,
//            style: TextStyle(
//              color: Colors.black,
//              fontFamily: 'Uthmanic',
//              fontSize: from == "1" ? textSize : textSize - 4,
//              fontWeight: FontWeight.w900,
//            ),
//          ),
//        ),
//      ),
//    );
//  }
//}
