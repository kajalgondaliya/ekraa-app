import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Word horof);

class Ch6Page10 extends StatefulWidget {
  final Chapter6Model model1;
  final HorofClickCallback onTap;

  const Ch6Page10({Key key, this.model1, this.onTap}) : super(key: key);

  @override
  _Ch6Page10State createState() => _Ch6Page10State();
}

class _Ch6Page10State extends State<Ch6Page10> {
  double tableHeight = 35;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            if (widget.model1.title.toString().trim().isNotEmpty)
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    Text(
                      widget.model1.title,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        color: ThemeColors.colorCoffee2,
                        fontFamily: 'Uthmanic',
                        fontSize: textSize + 5,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "Quranic script punctuation and signals for pausing",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: ThemeColors.colorBorderCoffee,
                        fontFamily: 'Uthmanic',
                        fontSize: textSize - 2,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )
            else
              SizedBox(),
            SizedBox(height: 20),
            getTable(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getTable() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      decoration: BoxDecoration(
        color: ThemeColors.colorTableBG,
        border: Border(
          top: BorderSide(width: 1, color: Colors.black),
          left: BorderSide(width: 1, color: Colors.black),
          right: BorderSide(width: 1, color: Colors.black),
        ),
      ),
      child: Column(
        children: <Widget>[
          getRow(false, widget.model1.filler.getRange(0, 3).toList()),
          getRow(false, widget.model1.filler.getRange(3, 6).toList()),
          getRow(false, widget.model1.filler.getRange(6, 9).toList()),
          getRow(false, widget.model1.filler.getRange(9, 12).toList()),
          getRow(false, widget.model1.filler.getRange(12, 15).toList()),
          getRow(true, widget.model1.filler.getRange(15, 18).toList()),
          getRow(true, widget.model1.filler.getRange(18, 21).toList()),
          getRow(true, widget.model1.filler.getRange(21, 24).toList()),
          getRow(false, widget.model1.filler.getRange(24, 27).toList()),
          getRow(false, widget.model1.filler.getRange(27, 30).toList()),
          getRow(false, widget.model1.filler.getRange(30, 33).toList()),
          getRow(false, widget.model1.filler.getRange(33, 36).toList()),
        ],
      ),
    );
  }

  getRow(bool split, List<Filler> filler) {
    double totalTableSize = MediaQuery.of(context).size.width - 40;
    double kOne = totalTableSize / 10;
    double kTwo = kDeviceType == DeviceScreenType.Mobile
        ? kOrientation ? 120 : 200
        : kOrientation ? 250 : 320;
    double kThree = totalTableSize - kTwo - kOne;

    return Container(
      height: kDeviceType == DeviceScreenType.Mobile
          ? kOrientation ? heightTable * 2 : heightTable * 2.3
          : heightTable * 2,
      child: ListView.builder(
        itemCount: filler.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          double size;

          if (index % 3 == 0) {
            size = kOne;
          } else {
            if (index % 2 == 0) {
              size = kThree;
            } else {
              size = kTwo;
            }
          }

          return itemTable(
              split, filler[index].wordsList, filler.length, index, size);
        },
      ),
    );
  }

  itemTable(
      bool split, List<Word> horofList, int size, int index, double kSize) {
//    print('horof list size index ===>> ${horofList.length}');

    return InkWell(
      onTap: () {
        if (horofList.length == 1) {
          widget.onTap(horofList[0]);
        }
      },
      child: Container(
        padding:
            index != 0 ? EdgeInsets.all(10) : EdgeInsets.zero,
        width: kSize,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(width: 1, color: Colors.black),
            bottom: BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: RichText(
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            text: TextSpan(
              children: _getListings(horofList),
            ),
          ),
        ),
      ),
    );
  }

  List<TextSpan> _getListings(List<Word> horofList) {
    List listings = List<TextSpan>();
    for (int i = 0; i < horofList.length; i++) {
      listings.add(
        TextSpan(
          text: horofList[i].word + " ",
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'Uthmanic',
            fontSize: textSize,
            fontWeight: FontWeight.w900,
          ),
          recognizer: TapGestureRecognizer()
            ..onTap = () {
              print('hello word ===>> ${horofList[i].word}');
              widget.onTap(horofList[i]);
            },
        ),
      );
    }

    return listings;
  }
}

/*itemTable(bool split, Filler horof, int size, int index, double kSize) {
  var horofList = horof.word.split("-");

//    print('horof list size index ===>> ${horofList.length}');

  return InkWell(
    onTap: () {
      widget.onTap(horof);
    },
    child: Container(
      padding:
      index != 0 ? EdgeInsets.symmetric(horizontal: 10) : EdgeInsets.zero,
      width: kSize,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(width: 1, color: Colors.black),
          bottom: BorderSide(width: 1, color: Colors.black),
        ),
      ),
      child: Center(
        child: !split
            ? Text(
          horof.word.trim(),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'Uthmanic',
            fontSize: textSize,
            fontWeight: FontWeight.w900,
          ),
        )
            : index == 2
            ? RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            children: _getListings(horof, horofList),
          ),
        )
            : Text(
          horof.word.trim(),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'Uthmanic',
            fontSize: textSize,
            fontWeight: FontWeight.w900,
          ),
        ),
      ),
    ),
  );
}

List<TextSpan> _getListings(Filler horof, List<String> horofList) {
  List listings = List<TextSpan>();
  for (int i = 0; i < horofList.length; i++) {
    listings.add(
      TextSpan(
        text: horofList[i],
        style: TextStyle(
          color: Colors.black,
          fontFamily: 'Uthmanic',
          fontSize: textSize,
          fontWeight: FontWeight.w900,
        ),
        recognizer: TapGestureRecognizer()
          ..onTap = () {
            print('hello word ===>> ${horofList[i]}');
            widget.onTap(horof);
          },
      ),
    );
  }

  return listings;
}*/
