import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch6Page11 extends StatefulWidget {
  final Chapter6Model model1;
  final HorofClickCallback onTap;

  const Ch6Page11({Key key, this.model1, this.onTap}) : super(key: key);

  @override
  _Ch6Page11State createState() => _Ch6Page11State();
}

class _Ch6Page11State extends State<Ch6Page11> {
  double tableHeight = 35;
  double totalTableSize;

  @override
  Widget build(BuildContext context) {
    totalTableSize = MediaQuery.of(context).size.width - 60;
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Text(
                    widget.model1.title,
                    textDirection: TextDirection.rtl,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: ThemeColors.colorCoffee2,
                      fontFamily: 'Uthmanic',
                      fontSize: textSize + 5,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "Quranic Script specifications",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: ThemeColors.colorBorderCoffee,
                      fontFamily: 'Uthmanic',
                      fontSize: textSize - 2,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        getTable2(),
                        SizedBox(height: 20),
                        getTable4(),
                      ],
                    ),
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        getTable1(),
                        SizedBox(height: 20),
                        getTable3(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getTable1() {
    return Container(
      color: ThemeColors.colorTableBG,
      child: Column(
        children: <Widget>[
          getTableHeaders(
            'الحروف الصغيرة تعامل معاملة الكبيرة',
            'Miniature letters is linguistically treated with big care.',
          ),
          Container(
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(width: 1, color: Colors.black),
                right: BorderSide(width: 1, color: Colors.black),
                top: BorderSide(width: 1, color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getWidget(widget.model1.filler.getRange(0, 2).toList()),
                getWidget(widget.model1.filler.getRange(2, 4).toList()),
                getWidget(widget.model1.filler.getRange(4, 6).toList()),
                getWidget(widget.model1.filler.getRange(6, 8).toList()),
                getWidget(widget.model1.filler.getRange(8, 10).toList()),
                getWidget(widget.model1.filler.getRange(10, 12).toList()),
                getWidget(widget.model1.filler.getRange(12, 14).toList()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getTable2() {
    return Container(
      color: ThemeColors.colorTableBG,
      child: Column(
        children: <Widget>[
          getTableHeaders(
            'إذا كان هناك حرف فوق حرف.. وكان الحرفان منفصلان.. ننطق الأعلى ونترك الأسفل',
            '3-If the two letters are above each other and they are disconnected, then only the above one is pronounced and the lower one is silent.',
          ),
          Container(
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(width: 1, color: Colors.black),
                right: BorderSide(width: 1, color: Colors.black),
                top: BorderSide(width: 1, color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getWidget(widget.model1.filler.getRange(14, 16).toList()),
                getWidget(widget.model1.filler.getRange(16, 18).toList()),
                getWidget(widget.model1.filler.getRange(18, 20).toList()),
                getWidget(widget.model1.filler.getRange(20, 22).toList()),
                getWidget(widget.model1.filler.getRange(22, 24).toList()),
                getWidget(widget.model1.filler.getRange(24, 26).toList()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getTable3() {
    return Container(
      color: ThemeColors.colorTableBG,
      child: Column(
        children: <Widget>[
          getTableHeaders(
            'إذا كان هناك حرف فوق حرف.. وكان الحرفان متصلان.. ننطق الاثنين ونبدأ بالحرف الأعلى',
            '2-If the two letters are above each other and connected, then both are pronounced starting with the above one.',
          ),
          Container(
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(width: 1, color: Colors.black),
                right: BorderSide(width: 1, color: Colors.black),
                top: BorderSide(width: 1, color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getWidget(widget.model1.filler.getRange(26, 28).toList()),
                getWidget(widget.model1.filler.getRange(28, 30).toList()),
                getWidget(widget.model1.filler.getRange(30, 32).toList()),
                getWidget(widget.model1.filler.getRange(32, 34).toList()),
                getWidget(widget.model1.filler.getRange(34, 36).toList()),
                getWidget(widget.model1.filler.getRange(36, 38).toList()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getTable4() {
    return Container(
      color: ThemeColors.colorTableBG,
      child: Column(
        children: <Widget>[
          getTableHeaders(
            'هناك حروف زائدة كتابة فلا تنطق أبدا.. نعرفها بوجود هذه العلامة فوق الحرف (ْ )',
            '4-There are letters that are written but not pronounced. They are silent letters and carry the mark (ْ)',
          ),
          Container(
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(width: 1, color: Colors.black),
                right: BorderSide(width: 1, color: Colors.black),
                top: BorderSide(width: 1, color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getWidget(widget.model1.filler.getRange(38, 40).toList()),
                getWidget(widget.model1.filler.getRange(40, 42).toList()),
                getWidget(widget.model1.filler.getRange(42, 44).toList()),
                getWidget(widget.model1.filler.getRange(44, 46).toList()),
                getWidget(widget.model1.filler.getRange(46, 48).toList()),
                getWidget(widget.model1.filler.getRange(48, 50).toList()),
                getWidget(widget.model1.filler.getRange(50, 52).toList()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getTableHeaders(String title1, String title2) {
    return Container(
      padding: EdgeInsets.all(30),
      child: Column(
        children: <Widget>[
          Text(
            title1,
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: kDeviceType == DeviceScreenType.Mobile
                  ? kOrientation ? 16 : 25
                  : kOrientation ? 25 : 30,
              fontWeight: FontWeight.w800,
            ),
          ),
          SizedBox(height: 5),
          Text(
            title2,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: kDeviceType == DeviceScreenType.Mobile
                  ? kOrientation ? 16 : 25
                  : kOrientation ? 25 : 30,
            ),
          ),
        ],
      ),
    );
  }

  getWidget(List<Filler> filler) {
    return Container(
      height: kOrientation ? heightTable + 10 : heightTable + 20,
      child: ListView.builder(
        itemCount: filler.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(filler[index], filler.length);
        },
      ),
    );
  }

  itemTable(Filler horof, int size) {
//    print('index ===>>> $index');

    return InkWell(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        width: totalTableSize / 4,

        decoration: BoxDecoration(
          border: Border(
//            top: BorderSide(width: 1, color: Colors.black),
            left: BorderSide(width: 1, color: Colors.black),
//            right: BorderSide(width: 1, color: Colors.black),
            bottom: BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
      ),
    );
  }
}
