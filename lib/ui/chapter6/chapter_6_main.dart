import 'dart:convert';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/ui/base_layout/base_home_layout.dart';
import 'package:ekraa_app/ui/chapter6/ch_6_lesson_1.dart';
import 'package:ekraa_app/ui/chapter6/ch_6_lesson_2.dart';
import 'package:ekraa_app/ui/chapter6/ch_6_lesson_4.dart';
import 'package:ekraa_app/ui/chapter6/ch_6_lesson_5.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:toast/toast.dart';

import 'ch_6_lesson_10.dart';
import 'ch_6_lesson_11.dart';
import 'ch_6_lesson_7.dart';
import 'ch_6_lesson_8_9.dart';

class Chapter6 extends StatefulWidget {
  @override
  _Chapter6State createState() => _Chapter6State();
}

class _Chapter6State extends State<Chapter6> {
  var _pageController = PageController();
  int page = 0;

  var currentLesson = "1";
  double currentTotalDuration = 0.0;
  double currentDuration = 0.0;

  List<dynamic> allLessons;

  List<Chapter6Model> lesson1List = [];
  List<Chapter6Model> lesson2List = [];
  List<Chapter6Model> lesson3List = [];
  List<Chapter6Model> lesson4List = [];
  List<Chapter6Model> lesson5List = [];
  List<Chapter6Model> lesson6List = [];
  List<Chapter6Model> lesson7List = [];
  List<Chapter6Model> lesson8List = [];
  List<Chapter6Model> lesson9List = [];
  List<Chapter6Model> lesson10List = [];
  List<Chapter6Model> lesson11List = [];

  AudioCache audioCache = AudioCache();

  bool isCompleted = false;

  String toastMsg = 'Audio is not avaiable.';

  refresh() {
    SchedulerBinding.instance.addPostFrameCallback((callback) {
      if (mounted) setState(() {});
    });
  }

  @override
  void initState() {
//    _assetsAudioPlayer.playlistAudioFinished.listen((data) {
//      print("finished : $data");
//    });

    audioCache.fixedPlayer = AudioPlayer();
    audioCache.fixedPlayer.onPlayerCompletion.listen((event) {
      print('complete event called ==>>');
      isCompleted = true;
      refresh();
    });
    audioCache.fixedPlayer.onDurationChanged.listen((data) {
//      print("current : ${data.duration}.");
//      print("current : ${data.duration}.");
      currentTotalDuration = durationToDouble(data);
//      print('currentTotalDuration : ${durationToDouble(data)}');
    });

    loadJsonFile();
    super.initState();
  }

  loadJsonFile() async {
    await rootBundle.loadString('assets/json/chapter6.json').then((onValue) {
      allLessons = jsonDecode(onValue); // all lessons list of chapter 6

      lesson1List
          .add(Chapter6Model.fromJson(allLessons[0])); // lesson 1 sub list
      lesson1List
          .add(Chapter6Model.fromJson(allLessons[1])); // lesson 1 sub list
      lesson1List
          .add(Chapter6Model.fromJson(allLessons[2])); // lesson 1 sub list

      lesson2List.add(Chapter6Model.fromJson(allLessons[3]));

      lesson3List.add(Chapter6Model.fromJson(allLessons[4]));
      lesson3List.add(Chapter6Model.fromJson(allLessons[5]));
      lesson3List.add(Chapter6Model.fromJson(allLessons[6]));
      lesson3List.add(Chapter6Model.fromJson(allLessons[7]));

      lesson4List.add(Chapter6Model.fromJson(allLessons[8]));

      lesson5List.add(Chapter6Model.fromJson(allLessons[9]));
      lesson5List.add(Chapter6Model.fromJson(allLessons[10]));

      lesson6List.add(Chapter6Model.fromJson(allLessons[10]));
      lesson6List.add(Chapter6Model.fromJson(allLessons[11]));

      lesson7List.add(Chapter6Model.fromJson(allLessons[12]));

      lesson8List.add(Chapter6Model.fromJson(allLessons[13]));

      lesson9List.add(Chapter6Model.fromJson(allLessons[14]));

      lesson10List.add(Chapter6Model.fromJson(allLessons[15]));

//      print('lesson 10 list ===>> ${lesson10List[0].toJson()}');

      lesson11List.add(Chapter6Model.fromJson(allLessons[16]));

      refresh();
    });
  }

  @override
  Widget build(BuildContext context) {
    // This function will set parameters device width,height,device_type,current_orientation,dynamic {textSize,heightTable,heightWords} according to screen size.
    // to know which variable holds which value please go to function declaration.
    // this is very essential as we are calculating above values dynamically according to device type and device orientation.
    setDeviceLayoutSizes(context);
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        body: BaseLayout(
          menuIcon: Icons.arrow_back,
          title1: 'Chapter',
          title2: ' 6',
          onTapHome: () {
            stopMusicPlayerAndGoBack();
          },
          onTapMenu: () {
            stopMusicPlayerAndGoBack();
          },
          child: getMainLayout(
            MediaQuery.of(context),
          ),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() async {
    audioCache.fixedPlayer.stop();
    return true; // return true if the route to be popped
  }

  stopMusicPlayerAndGoBack() {
    audioCache.fixedPlayer.stop();
    Navigator.of(context).pop();
  }

  getMainLayout(MediaQueryData mediaQueryData) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/white_page.jpg'),
          fit: BoxFit.fill,
        ),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            Expanded(
              child: PageView(
                controller: _pageController,
                physics: AlwaysScrollableScrollPhysics(),
                children: <Widget>[
                  lesson1List != null && lesson1List.isNotEmpty
                      ? Ch6Page1(
                          model1: lesson1List[0],
                          model2: lesson1List[1],
                          model3: lesson1List[2],
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                  lesson2List != null && lesson2List.isNotEmpty
                      ? Ch6Page2(
                          model1: lesson2List[0],
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                  lesson4List != null && lesson4List.isNotEmpty
                      ? Ch6Page4(
                          model1: lesson4List[0],
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                  lesson5List != null && lesson5List.isNotEmpty
                      ? Ch6Page5(
                          model1: lesson5List[0],
                          model2: lesson5List[1],
                          model3: lesson6List[0],
                          model4: lesson6List[1],
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
//                  lesson6List != null && lesson6List.isNotEmpty
//                      ? Ch6Page6(
//                          model1: lesson6List[0],
//                          model2: lesson6List[1],
//                          onTap: (value) {
////                            print('horof in home screen ===>> ${value.word}');
//                            if (value.media != null && value.media != "") {
//                              isCompleted = false;
//                              refresh();
//                              audioCache.play(value.media);
//                            } else {
//                              showToast(toastMsg);
//                            }
//                          },
//                        )
//                      : loader(),
                  lesson7List != null && lesson7List.isNotEmpty
                      ? Ch6Page7(
                          model1: lesson7List[0],
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            }
                          },
                        )
                      : loader(),
                  lesson8List != null && lesson8List.isNotEmpty
                      ? Ch6Page8(
                          model1: lesson8List[0],
                          fromLesson: "8",
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                  lesson9List != null && lesson9List.isNotEmpty
                      ? Ch6Page8(
                          model1: lesson9List[0],
                          fromLesson: "9",
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                  lesson10List != null && lesson10List.isNotEmpty
                      ? Ch6Page10(
                          model1: lesson10List[0],
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                  lesson11List != null && lesson11List.isNotEmpty
                      ? Ch6Page11(
                          model1: lesson11List[0],
                          onTap: (value) {
//                            print('horof in home screen ===>> ${value.word}');
                            if (value.media != null && value.media != "") {
                              isCompleted = false;
                              refresh();
                              audioCache.play(value.media);
                            } else {
//                              showToast(toastMsg);
                            }
                          },
                        )
                      : loader(),
                ],
                onPageChanged: (currentPage) {
                  page = currentPage;
                  currentLesson = "${page + 1}";
                  audioCache.fixedPlayer.stop();
                  refresh();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  String durationToString(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes =
        twoDigits(duration.inMinutes.remainder(Duration.minutesPerHour));
    String twoDigitSeconds =
        twoDigits(duration.inSeconds.remainder(Duration.secondsPerMinute));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  double durationToDouble(Duration duration) {
    var twoDigitMinutes = duration.inMinutes.remainder(Duration.minutesPerHour);
    var twoDigitSeconds =
        duration.inSeconds.remainder(Duration.secondsPerMinute);

    int totalDuration = (twoDigitMinutes * 60) + twoDigitSeconds;
    return double.parse(totalDuration.toString());
  }

  loader() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: 2, gravity: Toast.BOTTOM);
  }
}
