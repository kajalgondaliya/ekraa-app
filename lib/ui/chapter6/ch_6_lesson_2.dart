import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch6Page2 extends StatefulWidget {
  final Chapter6Model model1;
  final HorofClickCallback onTap;

  const Ch6Page2({Key key, this.model1, this.onTap}) : super(key: key);

  @override
  _Ch6Page2State createState() => _Ch6Page2State();
}

class _Ch6Page2State extends State<Ch6Page2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 20),
            getTable1(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getSquareLayout(String horof) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.all(3),
        width: 100,
        height: 45,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.red),
        ),
        child: Center(
          child: Text(
            horof,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.blue[800],
              fontFamily: 'Uthmanic',
              fontSize: textSize + 5,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }

  getTable1() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      height: kDeviceType == DeviceScreenType.Mobile
          ? kOrientation ? 520 : heightTable * 8.5
          : (height / 2) + 50,
      color: ThemeColors.colorTableBG,
      child: Column(
        children: <Widget>[
          Expanded(
            child: getTable1Header(
              "3- همزة الوصل في الأفعال والمصادر.. نلاحظ الحرف الثالث * نبدأ بضمة : لو كان الحرف الثالث مضموما * نبدأ بكسرة : لو كان الحرف الثالث مفتوحا أو مكسورا",
              "Hamzatulwasl in verbal nouns and derived forms of verbs is pronounced according to the short vowel of the third letter. " +
                  "If the third letter carries Damma, then hamzatulwasl will be pronounced with Damma. If the third letter carries " +
                  "Fat ha or Kasra, then hamzatulwasl will be pronounced with Kasra.",
            ),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(color: Colors.black),
                left: BorderSide(color: Colors.black),
                right: BorderSide(color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getRow(widget.model1.filler.getRange(0, 6).toList()),
                getRow(widget.model1.filler.getRange(6, 12).toList()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getTable1Header(String title, String title2) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          children: <Widget>[
            Text(
              title,
              textDirection: TextDirection.rtl,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'Uthmanic',
                fontSize: kDeviceType == DeviceScreenType.Mobile
                    ? kOrientation ? 16 : 20
                    : kOrientation ? 25 : 30,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: 10),
            Text(
              title2,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'Uthmanic',
                fontSize: kDeviceType == DeviceScreenType.Mobile
                    ? 16
                    : kOrientation ? 25 : 30,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }

  getRow(List row) {
    return Container(
      height: kOrientation ? heightTable + 10 : heightTable + 20,
      child: ListView.builder(
        itemCount: row.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(row[index], row.length);
        },
      ),
    );
  }

  itemTable(Filler horof, int size) {
    return InkWell(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.only(top: 3),
        width: (MediaQuery.of(context).size.width - 40) / size,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(width: 1, color: Colors.black),
            bottom: BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
