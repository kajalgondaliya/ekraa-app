import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch6Page5 extends StatefulWidget {
  final Chapter6Model model1;
  final Chapter6Model model2;
  final Chapter6Model model3;
  final Chapter6Model model4;
  final HorofClickCallback onTap;

  const Ch6Page5(
      {Key key, this.model1, this.model2, this.model3, this.model4, this.onTap})
      : super(key: key);

  @override
  _Ch6Page5State createState() => _Ch6Page5State();
}

class _Ch6Page5State extends State<Ch6Page5> {
  double tableHeight = 35;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            getMainLayout(),
            getTable1(),
            SizedBox(height: 20),
            getTable2(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getMainLayout() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Text(
                widget.model1.title,
                textDirection: TextDirection.rtl,
                style: TextStyle(
                  color: ThemeColors.colorCoffee2,
                  fontFamily: 'Uthmanic',
                  fontSize: textSize + 5,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "Laam qamariyah & Laam Shamsiyah",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ThemeColors.colorBorderCoffee,
                  fontFamily: 'Uthmanic',
                  fontSize: textSize - 2,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
//        Row(
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//          children: <Widget>[
//            Text(
//              "Read and notice the difference",
//              style: TextStyle(
//                color: Colors.black,
//                fontFamily: 'Uthmanic',
//                fontSize: kOrientation ? 14 : 22,
//                fontWeight: FontWeight.bold,
//              ),
//            ),
//            Text(
//              'أقرأ وﻻحظ الفرق',
//              textDirection: TextDirection.rtl,
//              style: TextStyle(
//                color: ThemeColors.colorCoffee2,
//                fontFamily: 'Uthmanic',
//                fontSize: kOrientation ? 16 : 25,
//                fontWeight: FontWeight.bold,
//              ),
//            ),
//          ],
//        ),
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            getSquareLayout(widget.model1.filler[0]),
            getSquareLayout(widget.model1.filler[1]),
          ],
        ),
        SizedBox(height: 20),
      ],
    );
  }

  getSquareLayout(Filler horof) {
    return GestureDetector(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.all(3),
        width: kDeviceType == DeviceScreenType.Mobile ? 110 : 180,
        height: kOrientation ? heightTable + 10 : heightTable + 20,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.red),
        ),
        child: Center(
          child: Text(
            horof.word,
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: kOrientation ? textSize : textSize + 4,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }

  getTable1() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      child: Column(
        children: <Widget>[
          Text(
            widget.model2.title,
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.red,
              fontFamily: 'Uthmanic',
              fontSize: 40,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 5),
          Text(
            "Laam Qamariyah",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: 35,
              fontWeight: FontWeight.w900,
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: ThemeColors.colorTableBG,
              border: Border(
                top: BorderSide(width: 1, color: Colors.black),
                right: BorderSide(width: 1, color: Colors.black),
                left: BorderSide(width: 1, color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getRow(widget.model2.filler.getRange(0, 6).toList()),
                getRow(widget.model2.filler.getRange(6, 12).toList()),
                getRow(widget.model2.filler.getRange(12, 14).toList()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getTable2() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      child: Column(
        children: <Widget>[
          Text(
            widget.model4.title,
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.red,
              fontFamily: 'Uthmanic',
              fontSize: 40,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 5),
          Text(
            "Laam Alshamsiyah",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: 35,
              fontWeight: FontWeight.w900,
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: ThemeColors.colorTableBG,
              border: Border(
                top: BorderSide(width: 1, color: Colors.black),
                right: BorderSide(width: 1, color: Colors.black),
                left: BorderSide(width: 1, color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getRow(widget.model4.filler.getRange(0, 6).toList()),
                getRow(widget.model4.filler.getRange(6, 12).toList()),
                getRow(widget.model4.filler.getRange(12, 14).toList()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getRow(List<dynamic> filler) {
    return Container(
      height: kOrientation ? heightTable + 10 : heightTable + 20,
      child: ListView.builder(
        itemCount: filler.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(filler[index], filler.length, index);
        },
      ),
    );
  }

  itemTable(Filler horof, int size, int index) {
//    print('index ===>>> $index');

    return InkWell(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.all(5),
        width: (MediaQuery.of(context).size.width - 40) / size,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(width: 1, color: Colors.black),
            bottom: BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
