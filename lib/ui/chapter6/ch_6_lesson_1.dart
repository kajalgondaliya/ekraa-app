import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch6Page1 extends StatefulWidget {
  final Chapter6Model model1;
  final Chapter6Model model2;
  final Chapter6Model model3;
  final HorofClickCallback onTap;

  const Ch6Page1({Key key, this.model1, this.model2, this.model3, this.onTap})
      : super(key: key);

  @override
  _Ch6Page1State createState() => _Ch6Page1State();
}

class _Ch6Page1State extends State<Ch6Page1> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: <Widget>[
                  Text(
                    widget.model1.title,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      color: ThemeColors.colorCoffee2,
                      fontFamily: 'Uthmanic',
                      fontSize: textSize + 5,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "Hammzat Alwasl",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: ThemeColors.colorBorderCoffee,
                      fontFamily: 'Uthmanic',
                      fontSize: textSize - 2,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                ],
              ),
            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Text(
//                  "Read and notice the difference",
//                  style: TextStyle(
//                    color: ThemeColors.colorCoffee2,
//                    fontFamily: 'Uthmanic',
//                    fontSize: kOrientation ? 14 : 22,
//                    fontWeight: FontWeight.bold,
//                  ),
//                ),
//                Text(
//                  'أقرأ وﻻحظ الفرق',
//                  textDirection: TextDirection.rtl,
//                  style: TextStyle(
//                    color: ThemeColors.colorCoffee2,
//                    fontFamily: 'Uthmanic',
//                    fontSize: kOrientation ? 16 : 25,
//                    fontWeight: FontWeight.bold,
//                  ),
//                ),
//              ],
//            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                getSquareLayout(widget.model1.filler[0]),
                getSquareLayout(widget.model1.filler[1]),
                getSquareLayout(widget.model1.filler[2]),
              ],
            ),
            /*SizedBox(height: 20),
            Column(
              children: <Widget>[
                Text(
                  'مَا يميز همزة الوصل :',
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    color: Colors.red,
                    fontFamily: 'Uthmanic',
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'ليست مشَكَّلة',
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    color: Colors.red,
                    fontFamily: 'Uthmanic',
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "'أقرأ وﻻحظ الفرق'",
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    color: Colors.red,
                    fontFamily: 'Uthmanic',
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  "What makes Hammzat Alwasl special is that: \n It is not a problem; It is not pronounced unless we start with the word carrying it.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Uthmanic',
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  "*كيف ننطق همزة الوصل إذا لم تكن مُشَكَّلة؟",
                  textAlign: TextAlign.center,
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    color: ThemeColors.colorBlue,
                    fontFamily: 'Uthmanic',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "How to pronounce Hammzat Alwasl when starting the recitation with it",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Uthmanic',
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),*/
            SizedBox(height: 20),
            getTable1(),
            SizedBox(height: 30),
            getTable2(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getSquareLayout(Filler horof) {
    return GestureDetector(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.all(3),
        width: kDeviceType == DeviceScreenType.Mobile ? 100 : 180,
        height: kOrientation ? heightTable + 10 : heightTable + 20,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.red),
        ),
        child: Center(
          child: Text(
            horof.word,
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: kOrientation ? textSize + 2 : textSize + 4,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }

  getTable1Header(String title, String title2) {
    return Container(
      padding: EdgeInsets.all(25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'Uthmanic',
                fontSize: kDeviceType == DeviceScreenType.Mobile
                    ? kOrientation ? 16 : 25
                    : kOrientation ? 25 : 30,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Text(
              title2,
              textDirection: TextDirection.rtl,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'Uthmanic',
                fontSize: kDeviceType == DeviceScreenType.Mobile
                    ? kOrientation ? 16 : 25
                    : kOrientation ? 25 : 30,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ],
      ),
    );
  }

  getTable1() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      height: kDeviceType == DeviceScreenType.Mobile
          ? kOrientation ? heightTable * 6 : heightTable * 8
          : kOrientation ? heightTable * 5 : heightTable * 6,
      color: ThemeColors.colorTableBG,
      child: Column(
        children: <Widget>[
          Expanded(
            child: getTable1Header(
              'Hamzatulwasl that \nprecedes Lam is \npronounced with Fat ha',
              'همزة الوصل التي بعدها لام تنطق فتحة (اَل)',
            ),
          ),
          Container(
            decoration: BoxDecoration(
//              border: Border.all(color: Colors.black),
              border: Border(
                top: BorderSide(color: Colors.black),
                left: BorderSide(color: Colors.black),
                right: BorderSide(color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getRow(widget.model2.filler.getRange(0, 6).toList()),
                getRow(widget.model2.filler.getRange(6, 12).toList()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getTable2() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      height: kDeviceType == DeviceScreenType.Mobile
          ? kOrientation ? heightTable * 11 : heightTable * 13
          : kOrientation ? heightTable * 9 : heightTable * 10,
      color: ThemeColors.colorTableBG,
//      height: kOrientation ? heightTable * 9 : heightTable * 11,
//      decoration: BoxDecoration(
//        color: ThemeColors.colorGreyLight,
////        border: Border.all(color: Colors.black),
//      ),
      child: Column(
        children: <Widget>[
          Expanded(
            child: getTable1Header(
                'Hamzatulwasl in nouns is pronounced with Kasra',
                'همزة الوصل في الأسماء مكسورة (اِ)'),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(color: Colors.black),
                left: BorderSide(color: Colors.black),
                right: BorderSide(color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getRow(widget.model3.filler.getRange(0, 6).toList()),
                getRow(widget.model3.filler.getRange(6, 13).toList()),
                getRow(widget.model3.filler.getRange(13, 18).toList()),
                getRow(widget.model3.filler.getRange(18, 20).toList()),
                getRow(widget.model3.filler.getRange(20, 22).toList()),
                getRow(widget.model3.filler.getRange(22, 24).toList()),
              ],
            ),
          )
        ],
      ),
    );
  }

  getRow(List row1) {
    return Container(
      height: kOrientation ? heightTable + 10 : heightTable + 20,
      child: ListView.builder(
        itemCount: row1.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(row1[index], row1.length, index);
        },
      ),
    );
  }

  itemTable(Filler horof, int size, int index) {
    return InkWell(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.only(top: 3),
        width: (MediaQuery.of(context).size.width - 40) / size,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: index != size - 1
                ? BorderSide(width: 1, color: Colors.black)
                : BorderSide.none,
            bottom: BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
