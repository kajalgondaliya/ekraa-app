import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch6Page6 extends StatefulWidget {
  final Chapter6Model model1;
  final Chapter6Model model2;
  final HorofClickCallback onTap;

  const Ch6Page6({Key key, this.model1, this.model2, this.onTap})
      : super(key: key);

  @override
  _Ch6Page6State createState() => _Ch6Page6State();
}

class _Ch6Page6State extends State<Ch6Page6> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            getMainLayout(),
            getTable(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getMainLayout() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Text(
            widget.model1.title,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: ThemeColors.colorCoffee2,
              fontFamily: 'Uthmanic',
              fontSize: textSize + 5,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(height: 20),
        /*Column(
          children: <Widget>[
            Text(
              "هي لام (ٱل) إذا جاء بعدها حرف من هذه الحروف \n  (ت ث د ذ ر ز س ش ص ض ط ظ ل ن)",
              textDirection: TextDirection.rtl,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.red,
                fontFamily: 'Uthmanic',
                fontSize: kOrientation ? 16 : 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Text(
              "It is the Laam followed by those letters",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'Uthmanic',
                fontSize: kOrientation ? 14 : 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: 5),
            Text(
              "(ت ث د ذ ر ز س ش ص ض ط ظ ل ن)",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'Uthmanic',
                fontSize: kOrientation ? 16 : 18,
                fontWeight: FontWeight.w600,
                letterSpacing: 1.0,
              ),
            ),
          ],
        ),
        SizedBox(height: 20),*/
      ],
    );
  }

  getTable() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      color: ThemeColors.colorTableBG,
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(width: 1, color: Colors.black),
                right: BorderSide(width: 1, color: Colors.black),
                left: BorderSide(width: 1, color: Colors.black),
              ),
            ),
            child: Column(
              children: <Widget>[
                getRow(widget.model2.filler.getRange(0, 6).toList()),
                getRow(widget.model2.filler.getRange(6, 12).toList()),
                getRow(widget.model2.filler.getRange(12, 14).toList()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getRow(List<dynamic> filler) {
    return Container(
      height: kOrientation ? heightTable + 10 : heightTable + 20,
      child: ListView.builder(
        itemCount: filler.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(filler[index], filler.length, index);
        },
      ),
    );
  }

  itemTable(Filler horof, int size, int index) {
//    print('index ===>>> $index');

    return InkWell(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.only(top: 3),
        width: (MediaQuery.of(context).size.width - 40) / size,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(width: 1, color: Colors.black),
            bottom: BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black87,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
