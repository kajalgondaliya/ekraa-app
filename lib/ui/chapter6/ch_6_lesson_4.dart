import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/ui/chapter6/chapter_6_main.dart';
import 'package:flutter/material.dart';
import 'package:ekraa_app/ui/select_chapters/select_chapter.dart';
import 'package:ekraa_app/utils/ui_utils.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch6Page4 extends StatefulWidget {
  final Chapter6Model model1;
  final HorofClickCallback onTap;

  const Ch6Page4({Key key, this.model1, this.onTap}) : super(key: key);

  @override
  _Ch6Page4State createState() => _Ch6Page4State();
}

class _Ch6Page4State extends State<Ch6Page4> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Text(
                    widget.model1.title,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      color: ThemeColors.colorCoffee2,
                      fontFamily: 'Uthmanic',
                      fontSize: textSize + 5,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "Exercise on Hamza Alwasl",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: ThemeColors.colorBorderCoffee,
                      fontFamily: 'Uthmanic',
                      fontSize: textSize - 2,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            getTable(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getTable() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      decoration: BoxDecoration(
        color: ThemeColors.colorTableBG,
        border: Border(
          top: BorderSide(width: 1, color: Colors.black),
          left: BorderSide(width: 1, color: Colors.black),
          right: BorderSide(width: 1, color: Colors.black),
        ),
      ),
      child: Column(
        children: <Widget>[
          getRow(widget.model1.filler.getRange(0, 4).toList()),
          getRow(widget.model1.filler.getRange(4, 8).toList()),
          getRow(widget.model1.filler.getRange(8, 12).toList()),
          getRow(widget.model1.filler.getRange(12, 16).toList()),
          getRow(widget.model1.filler.getRange(16, 20).toList()),
        ],
      ),
    );
  }

  getRow(List<dynamic> filler) {
    return Container(
      height: kOrientation ? heightTable + 10 : heightTable + 20,
      child: ListView.builder(
        itemCount: filler.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(filler[index], filler.length, index);
        },
      ),
    );
  }

  itemTable(Filler horof, int size, int index) {
//    print('index ===>>> $index');

    return InkWell(
      onTap: () {
        widget.onTap(horof);
      },
      child: Container(
        padding: EdgeInsets.only(top: 3),
        width: (MediaQuery.of(context).size.width - 40) / size,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(width: 1, color: Colors.black),
            bottom: BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
