import 'package:ekraa_app/model/chapter/chapter6/chapter6Model.dart';
import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/utils/ui_utils.dart';
import 'package:flutter/material.dart';

typedef HorofClickCallback = Function(Filler horof);

class Ch6Page8 extends StatefulWidget {
  final Chapter6Model model1;
  final HorofClickCallback onTap;
  final fromLesson;

  const Ch6Page8({Key key, this.model1, this.fromLesson, this.onTap})
      : super(key: key);

  @override
  _Ch6Page8State createState() => _Ch6Page8State();
}

class _Ch6Page8State extends State<Ch6Page8> {
  double tableHeight = 35;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            if (widget.model1.title.toString().trim().isNotEmpty)
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    Text(
                      widget.model1.title,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        color: ThemeColors.colorCoffee2,
                        fontFamily: 'Uthmanic',
                        fontSize: textSize + 5,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      widget.fromLesson == "8"
                          ? "How to stop the recitation"
                          : "",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: ThemeColors.colorBorderCoffee,
                        fontFamily: 'Uthmanic',
                        fontSize: textSize - 2,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )
            else
              SizedBox(
                height: 20,
              ),
            SizedBox(height: 20),
            getTable(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  getTable() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      decoration: BoxDecoration(
        color: ThemeColors.colorTableBG,
        border: Border(
          top: BorderSide(width: 1, color: Colors.black),
          left: BorderSide(width: 1, color: Colors.black),
          right: BorderSide(width: 1, color: Colors.black),
        ),
      ),
      child: Column(
        children: <Widget>[
          getRow(true, widget.model1.filler.getRange(0, 3).toList()),
          getRow(true, widget.model1.filler.getRange(3, 6).toList()),
          getRow(true, widget.model1.filler.getRange(6, 9).toList()),
          getRow(true, widget.model1.filler.getRange(9, 12).toList()),
          getRow(true, widget.model1.filler.getRange(12, 15).toList()),
          getRow(true, widget.model1.filler.getRange(15, 18).toList()),
          getRow(true, widget.model1.filler.getRange(18, 21).toList()),
          getRow(false, widget.model1.filler.getRange(21, 24).toList()),
          getRow(false, widget.model1.filler.getRange(24, 27).toList()),
          getRow(true, widget.model1.filler.getRange(27, 30).toList()),
          getRow(true, widget.model1.filler.getRange(30, 33).toList()),
        ],
      ),
    );
  }

  getRow(bool shouldPaint, List<dynamic> filler) {
    return Container(
      height: kDeviceType == DeviceScreenType.Mobile
          ? kOrientation ? heightTable * 2 : heightTable * 2.5
          : heightTable * 2,
      child: ListView.builder(
        itemCount: filler.length,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        reverse: true,
        itemBuilder: (context, index) {
          return itemTable(shouldPaint, filler[index], filler.length, index);
        },
      ),
    );
  }

  itemTable(bool border, Filler horof, int size, int index) {
//    print('index ===>>> $index');

    return InkWell(
      onTap: () {
        if (index != 0) {
          widget.onTap(horof);
        }
      },
      child: Container(
        padding: EdgeInsets.all(10),
        width: (MediaQuery.of(context).size.width - 40) / size,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(width: 1, color: Colors.black),
            bottom: widget.fromLesson == "8"
                ? index == 0
                    ? border
                        ? BorderSide(width: 1, color: Colors.black)
                        : BorderSide.none
                    : BorderSide(width: 1, color: Colors.black)
                : BorderSide(width: 1, color: Colors.black),
          ),
        ),
        child: Center(
          child: Text(
            horof.word,
            textAlign: TextAlign.center,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Uthmanic',
              fontSize: textSize,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
      ),
    );
  }
}
