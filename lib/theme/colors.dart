import 'package:flutter/material.dart';

class ThemeColors {
  static var colorBrown = const Color(0xff380D07);
  static var colorLightBrown = const Color(0xff9A7D6E);
  static var colorLightDullBrown = const Color(0xff574B3E);
  static var colorCoffee = const Color(0xff534741);
  static var colorLightCoffee = const Color(0xffC7B299);
  static var colorBorderCoffee = const Color(0xffA39162);
  static var colorCoffee2 = const Color(0xffB86351);
  static var colorGreyLight = const Color(0xffDCDCDC);
  static var colorBlue = const Color(0xff0000FF);
  static var colorRose = const Color(0xff914046);
  static var colorTableBG = const Color(0xfff5f1e6);
}
