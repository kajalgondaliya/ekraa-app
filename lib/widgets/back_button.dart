import 'package:ekraa_app/theme/colors.dart';
import 'package:flutter/material.dart';

class CommonBackButton extends StatelessWidget {
  final onTap;
  final negativeMargin;

  const CommonBackButton({
    Key key,
    this.onTap,
    this.negativeMargin = 0.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(2),
        width: 40,
        height: 40,
        transform: Matrix4.translationValues(0.0, -negativeMargin, 0.0),
        decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(color: ThemeColors.colorBrown)),
        child: Container(
          padding: EdgeInsets.all(7),
          decoration: BoxDecoration(
            color: ThemeColors.colorLightDullBrown,
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Image.asset('assets/icons/ic_back.png'),
        ),
      ),
    );
  }
}
