import 'package:ekraa_app/theme/colors.dart';
import 'package:ekraa_app/ui/select_chapters/select_chapter.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  static const id = 'Home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation animation;

  void initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(
        seconds: 1,
      ),
      vsync: this,
      upperBound: 1,
    );
    animation = ColorTween(begin: Colors.blueGrey, end: Colors.white)
        .animate(controller);
    controller.forward();

    controller.addListener(() {
      setState(() {});
      print(animation.value);
    });
  }

  @override
//   void dispose(){
//    controller.dispose();
//       super.dispose();
//   }
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: animation.value,

      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/back2.png"),
            fit: BoxFit.cover,
          ),
        ),

        // Padding(
        //padding: EdgeInsets.symmetric(horizontal: 24.0),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(50.0),
              child: Material(
                color: ThemeColors.colorCoffee2,
                borderRadius: BorderRadius.circular(30.0),
                elevation: 5.0,
                child: MaterialButton(
                  onPressed: () {
                    Navigator.pushNamed(context, SelectChapter.id);
                  },
                  minWidth: 200.0,
                  height: 42.0,
                  child: Text(
                    'Lessons',
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
