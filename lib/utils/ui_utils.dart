import 'package:ekraa_app/sizing_info/device_screen_type.dart';
import 'package:flutter/widgets.dart';

double width, height, heightTable, heightWords, textSize = 18;

var kOrientation, kDeviceType;

MediaQueryData mediaQueryData;

DeviceScreenType kGetDeviceType(MediaQueryData mediaQuery) {
  double deviceWidth = mediaQuery.size.shortestSide;

  if (deviceWidth > 950) {
    return DeviceScreenType.Desktop;
  }

  if (deviceWidth > 600) {
    return DeviceScreenType.Tablet;
  }

  return DeviceScreenType.Mobile;
}

bool kGetOrientation(MediaQueryData mediaQueryData, BuildContext context) {
  var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;

  // true for portrait // else landscape

  return isPortrait;
}

void setDeviceLayoutSizes(BuildContext context) {
  mediaQueryData = MediaQuery.of(context);

  // it will return
  // Portrait = true
  // Landscape = false
  kOrientation = kGetOrientation(mediaQueryData, context);

  // it will return
  // Mobile = true
  // Tablet = false

  kDeviceType = kGetDeviceType(MediaQuery.of(context));

//  print('current device type ===>>> $kDeviceType');

  if (kOrientation) {
    /// if orientation is in PORTRAIT MODE

    // if its portrait mode device width will behave as defaults which is width of the device same for height.
    width = mediaQueryData.size.width;
    height = mediaQueryData.size.height;
    heightTable =
        width / 9; // dynamic height of table row according to device width
    heightWords =
        width / 9; // dynamic height of words shape according to device width

    /// if DEVICE_TYPE is MOBILE
    if (kDeviceType == DeviceScreenType.Mobile) {
      textSize = width /
          21; // dynamic text size according to device width for device type mobile
    } else {
      /// if DEVICE_TYPE is TABLET
      textSize = width /
          19; // dynamic text size according to device width for device type tablet
    }
  } else {
    /// if orientation is in LANDSCAPE MODE

    //but if its landscape mode device width will behave as height of the device and device height will be the width of the device.

    width = mediaQueryData.size.height;
    height = mediaQueryData.size.width;
    heightTable = width / 9;
    heightWords = width / 9;
    if (kDeviceType == DeviceScreenType.Mobile) {
      textSize = width / 15;
    } else {
      textSize = width / 19;
    }

  }
}
