class LessonsAll {
  final List<Filler> filler;
  final int page; // 23
  final String source; // ../../assets/lessons/Ekraa final23.png
  final String subTitle; // الضمة
  final String title; // الدرس 1
  final String type; // template
  final String view; // vi

  List<Filler> row1 = [];
  List<Filler> row2 = [];
  List<Filler> row3 = [];
  List<Filler> row4 = [];
  List<Filler> tab1 = [];
  List<Filler> tab2 = [];
  List<Filler> tab3 = [];
  List<Filler> tab4 = [];
  List<Filler> tab5 = [];
  List<Filler> tab6 = [];

  LessonsAll(
      {this.filler,
      this.page,
      this.source,
      this.subTitle,
      this.title,
      this.type,
      this.view});

  factory LessonsAll.fromJson(Map<String, dynamic> json) {
    return LessonsAll(
      filler: json['filler'] != null
          ? (json['filler'] as List).map((i) => Filler.fromJson(i)).toList()
          : null,
      page: json['page'],
      source: json['source'],
      subTitle: json['subTitle'],
      title: json['title'],
      type: json['type'],
      view: json['view'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['source'] = this.source;
    data['subTitle'] = this.subTitle;
    data['title'] = this.title;
    data['type'] = this.type;
    data['view'] = this.view;
    if (this.filler != null) {
      data['filler'] = this.filler.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Filler {
  final String alpha; // يُ
  final String
      media; // ../../../../assets/sounds/bab2/ALDUMMAH/ALDUMMAH_WORDS/30.mp3
  final String word; // وَكُتُبِهِ

  Filler({this.alpha, this.media, this.word});

  factory Filler.fromJson(Map<String, dynamic> json) {
    return Filler(
      alpha: json['alpha'],
      media: json['media'],
      word: json['word'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['alpha'] = this.alpha;
    data['media'] = this.media;
    data['word'] = this.word;
    return data;
  }
}
