class Chapter {
  final String name;
  final String number;

  Chapter({this.name, this.number});
}
