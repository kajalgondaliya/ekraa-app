class Chapter6Model {
  final List<Filler> filler;
  final String media; // ../../../../assets/sounds/bab6/Rasm_Almoshaf/22.mp3
  final int page; // 46
  final String source; // ../../assets/lessons/Ekraa final44.png
  final String title; // من خصوصيات رسم المصحف و خطه
  final String subTitle; // من خصوصيات رسم المصحف و خطه
  final String type; // template
  final String view; // view14
  final String word; // يَبۡدَؤُ

  Chapter6Model(
      {this.filler,
      this.media,
      this.page,
      this.source,
      this.title,
      this.subTitle,
      this.type,
      this.view,
      this.word});

  factory Chapter6Model.fromJson(Map<String, dynamic> json) {
    return Chapter6Model(
      filler: json['filler'] != null
          ? (json['filler'] as List).map((i) => Filler.fromJson(i)).toList()
          : null,
      media: json['media'],
      page: json['page'],
      source: json['source'],
      title: json['title'],
      subTitle: json['subTitle'],
      type: json['type'],
      view: json['view'],
      word: json['word'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['media'] = this.media;
    data['page'] = this.page;
    data['source'] = this.source;
    data['title'] = this.title;
    data['subTitle'] = this.subTitle;
    data['type'] = this.type;
    data['view'] = this.view;
    data['word'] = this.word;
    if (this.filler != null) {
      data['filler'] = this.filler.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Filler {
  final String media; // sounds/bab6/Rasm_Almoshaf/22.mp3
  final String word; // يَبۡدَؤُ
  final String alpha; // ءَا۬عۡجَمِيّٞ
  final List<Word> wordsList;

  Filler({this.media, this.word, this.alpha, this.wordsList});

  factory Filler.fromJson(Map<String, dynamic> json) {
    return Filler(
      media: json['media'],
      word: json['word'],
      alpha: json['alpha'],
      wordsList: json['wordsList'] != null
          ? (json['wordsList'] as List).map((i) => Word.fromJson(i)).toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['media'] = this.media;
    data['word'] = this.word;
    data['alpha'] = this.alpha;
    if (this.wordsList != null) {
      data['wordsList'] = this.wordsList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Word {
  final String media; // sounds/bab6/Rasm_Almoshaf/22.mp3
  final String word; // يَبۡدَؤُ

  Word({this.media, this.word});

  factory Word.fromJson(Map<String, dynamic> json) {
    return Word(
      media: json['media'],
      word: json['word'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['media'] = this.media;
    data['word'] = this.word;
    return data;
  }
}
