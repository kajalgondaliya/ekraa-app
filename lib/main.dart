import 'package:ekraa_app/Home.dart';
import 'package:ekraa_app/ui/chapter1/chapter_1_main.dart';
import 'package:ekraa_app/ui/chapter2/chapter_2_main.dart';
import 'package:ekraa_app/ui/chapter3/chapter_3_main.dart';
import 'package:ekraa_app/ui/chapter4/chapter_4_main.dart';
import 'package:ekraa_app/ui/chapter5/chapter_5_main.dart';
import 'package:ekraa_app/ui/chapter6/chapter_6_main.dart';
import 'package:ekraa_app/ui/select_chapters/select_chapter.dart';
import 'package:ekraa_app/utils/app_routes.dart';
import 'package:ekraa_app/widgets/MyBehavior.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
//    SystemChrome.setPreferredOrientations([
//      DeviceOrientation.portraitUp,
//    ]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, child) {
        return ScrollConfiguration(
          behavior: MyBehavior(),
          child: child,
        );
      },
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Home(),
      ),
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case AppRoutes.chapter1:
            return PageTransition(
              child: Chapter1(),
              type: PageTransitionType.rightToLeft,
              settings: settings,
            );
            break;
          case AppRoutes.chapter2:
            return PageTransition(
              child: Chapter2(),
              type: PageTransitionType.rightToLeft,
              settings: settings,
            );
            break;
          case AppRoutes.chapter3:
            return PageTransition(
              child: Chapter3(),
              type: PageTransitionType.rightToLeft,
              settings: settings,
            );
            break;
          case AppRoutes.chapter4:
            return PageTransition(
              child: Chapter4(),
              type: PageTransitionType.rightToLeft,
              settings: settings,
            );
            break;
          case AppRoutes.chapter5:
            return PageTransition(
              child: Chapter5(),
              type: PageTransitionType.rightToLeft,
              settings: settings,
            );
            break;
          case AppRoutes.chapter6:
            return PageTransition(
              child: Chapter6(),
              type: PageTransitionType.rightToLeft,
              settings: settings,
            );
            break;
          default:
            return null;
        }
      },
      routes: {
        Home.id: (context) => Home(),
        SelectChapter.id: (context) => SelectChapter(),
      },
    );
  }
}
